



<section class="mainpart">
    <!-- /Section-heading start -->
    <section class="section-heading">
        <div class="container">
            <h1>%pageTitle%</h1>
        </div>
    </section>
    <!-- /Section-heading over -->
    <!-- /Section-middle start -->
    <section class="my-provider-hired">
        <div class="container">
            
                %pageDesc%
            
        </div>
    </section>
    <!-- /Section-middle over -->
</section>
