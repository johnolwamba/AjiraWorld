<div class="tbody">
	<div data-title="{Requested_Amount} (%currency_sign%)" class="td">
		%amount_calculated%
	</div>
	<div data-title="{Requested_Date}" class="td">
		%createdDate%
	</div>
	<div data-title="{Description}" class="td">
		%description%
	</div>
	<div data-title="Redemption Status" class="td">
		%paymentStatus%
	</div>
	<div data-title="{Redeemed_Amount} (%currency_sign%)" class="td">
		%redeemedAmount_calculated%
	</div>
	<div data-title="{Redeemed_Date}" class="td">
		%redeemedDate%
	</div>
</div>