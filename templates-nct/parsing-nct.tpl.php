<?php
	
	$main->set("module", $module);
	require_once(DIR_THEME.'theme.template.php');

  	/* Loading template files */

	/* for head  start*/
	$search = array('%METATAG%','%TITLE%');
	$replace = array($metaTag,$winTitle);
	$head_content=str_replace($search,$replace,$head->parse());	
	/* for head  end*/

    /* Outputting the data to the end user */
	$search = array('%HEAD%','%SITE_HEADER%','%BODY%','%FOOTER%','%MESSAGE_TYPE%');
	$replace = array($head_content,$objHome->getHeaderContent($module),$pageContent,$objHome->getFooterContent(),$msgType);
	$page_content=str_replace($search,$replace,$page->parse());
    echo ($page_content);
	exit;
