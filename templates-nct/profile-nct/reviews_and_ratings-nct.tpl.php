<div class="review-section" id="review-section">
	<div class="review-tit">
		Reviews and Ratings
	</div>
	<div class="review-desc" data-infinite="container">
		%rows%
	</div>
</div>
