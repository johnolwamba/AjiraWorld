<!-- Redeem Popup start -->
<div class="modal fade" id="gateWayModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header redeem-modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
                <h3>{Choose_Gateway}</h3>
            </div>
            <div class="modal-body redeerm-modal-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="sign_up_form">
                            <div class="row">
                                <form id="chooseGateway" method="post" action="{SITE_URL}ajax-membership-nct/" autocomplete="off">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                        <input type="radio" class="form-group" value="pa"
                                            name="gateway"
                                            id="1"
                                            data-validation="required" %PAYCHECKED%>
                                            <img for="1" src="{SITE_IMG}paypal-logo.png" alt="">

                                        <input type="radio" class="form-group" value="pe"
                                            name="gateway"
                                            id="2"
                                            data-validation="required" %PESACHECKED%>
                                            <img for="2" src="{SITE_IMG}pepal.png" alt="">
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="hidden" name="action" value="buy"/>
                                            <input type="hidden" name="id" id="m_plan" value=""/>
                                            <input data-ele="chooseGateway" type="submit" name="chooseGateway" class="btn btn_blue btn-block btn_light_hover" value="Buy" tabindex="2">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {Close}
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Redeem Popup End -->