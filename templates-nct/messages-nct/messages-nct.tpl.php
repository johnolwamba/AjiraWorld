<section class="mainpart">
	<!-- /Section-heading start -->
	<section class="section-heading">
		<div class="container">
			<h1>Messages</h1>
		</div>
	</section>
	<section class="all-message-main">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="project-bottom-part">
						<div class="white-box">
							<div class="clearfix"></div>
							<div class="workroom-messages clearfix">
								<div class="msg-tabs msg-tab-box">
									<div class="col-sm-5 col-md-4 clearfix">
										<div id="content-2" class="all-msg-tab mCustomScrollbar">
											<ul class="nav nav-tabs ">
											    %left%												
											</ul>
										</div>
									</div>
									<div class="col-sm-7 col-md-8">
										<div class="tab-content">
											<div id="messages" class="tab-pane fade in active">
												<div id="content-2" class="all-msg-inbox mCustomScrollbar">
													<div class="msg-chat-main">
														<div class="msg-inbox-chat">
															<ul class="chat-row" data-ele="tab_panel">
																%right%
															</ul>
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												<!--
												<form class="chat-form">
													<div class="form-group">
														<textarea class="form-control" rows="2" placeholder="type a message here..."></textarea>
													</div>
													<div class="form-group text-right">
														<button class="btn btn-link">
															Send
														</button>
													</div>
												</form>-->
												
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
