<section class="mainpart">
	<section class="section-heading">
		<div class="container">
			<h1 class="font_bold marginbtm30">{SITE_NM} continues to draw on talents from across the globe</h1>
		</div>
	</section>
	<section class="section-middle">
		<div class="top_skills_all">
			<div class="top_skills marginbtm30">
				<div class="container">				
					<div class="row margintop10 wow fadeInUp">
						%TOP_SKILLS%
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
