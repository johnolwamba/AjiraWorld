<li class="milestones-cell %current%" data-infinite="row">
	<h4>{Milestone} %iterator%</h4>
	<ul class="price-date">
		<li>
			<h5>%price_calculated%</h5>
		</li>
		<li>
			{Delivery_Date} <strong>%milestone_date%</strong>
		</li>
	</ul>
	<p>
		%description%
	</p>
	<div class="mt-right %righttick%">
		%btn%
	</div>
</li>
