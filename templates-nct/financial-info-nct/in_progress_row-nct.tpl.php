<div class="tbody">
	<div data-title="{Project_Details}" class="td">
		<div class="financial-row">
			<a href="%profileLink%" target="_blank" class="financial-img"><img src="%profilePhoto%" title="%fullName%" alt="%fullName%"></a>
			<a href="%profileLink%" target="_blank" class="financial-user">%fullName%</a>
			<h4><a href="%projLink%" target="_blank">%title%</a></h4>
		</div>
	</div>
	<div data-title="{Total} %total_price%" class="td td-blue">
		%total%
	</div>
	<div data-title="%paid_or_earned% %total_paid%" class="td td-light-blue">
		%paid%
	</div>
	<div data-title="{Outstanding} %total_outstanding%" class="td td-dark-gray">
		%outstanding%
	</div>
</div>