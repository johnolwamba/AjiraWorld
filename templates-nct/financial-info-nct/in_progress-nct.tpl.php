<div class="financial-info">
	<div class="financial-info-table">
		<div class="hist-tables">
			<div class="thead">
				<div class="th">
					{Project_Details}
				</div>
				<div class="th dark-blue">
					Total
					<span>
						%total_price%
					</span>
				</div>
				<div class="th light-blue">
					%paid_or_earned%
					<span>
						%total_paid%
					</span>
				</div>
				<div class="th dark-gray">
					{Outstanding}
					<span>
						%total_outstanding%
					</span>
				</div>
			</div>
			%row%
		</div>
	</div>
</div>
