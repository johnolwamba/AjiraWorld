<section class="no_rec_section" style="opacity: 0.2">
	<div class="financial-info">
		<div class="financial-info-table">
			<div class="hist-tables">
				<div class="thead">
					<div class="th">
						{Project_Details}
					</div>
					<div class="th dark-blue">
						Total
						<span>
							$2500
						</span>
					</div>
					<div class="th light-blue">
						Paid
						<span>
							$1000
						</span>
					</div>
					<div class="th dark-gray">
						{Outstanding}
						<span>
							$1500
						</span>
					</div>
				</div>
				<div class="tbody">
					<div data-title="{Project_Details}" class="td">
						<div class="financial-row">
							<a href="#" class="financial-img" title="%fullName%"><img src="{SITE_IMG}user1.jpg" alt=""></a>
							<a href="#" class="financial-user">Don Jonny</a>
							<h4>Design a Logo for Spud on Stick for US Company </h4>
						</div>
					</div>
					<div data-title="Total $2500" class="td td-blue">
						$1100
					</div>
					<div data-title="Paid $1000" class="td td-light-blue">
						$700
					</div>
					<div data-title="{Outstanding} $1500" class="td td-dark-gray">
						20
					</div>
				</div>
				<div class="tbody">
					<div data-title="{Project_Details}" class="td">
						<div class="financial-row">
							<a href="#" class="financial-img" title="%fullName%"><img src="{SITE_IMG}user1.jpg" alt=""></a>
							<a href="#" class="financial-user">Don Jonny</a>
							<h4>Design a Logo for Spud on Stick for US Company </h4>
						</div>
					</div>
					<div data-title="Total $2500" class="td td-blue">
						$1100
					</div>
					<div data-title="Paid $1000" class="td td-light-blue">
						$700
					</div>
					<div data-title="{Outstanding} $1500" class="td td-dark-gray">
						20
					</div>
				</div>
				<div class="tbody">
					<div data-title="{Project_Details}" class="td">
						<div class="financial-row">
							<a href="#" class="financial-img" title="%fullName%"><img src="{SITE_IMG}user1.jpg" alt=""></a>
							<a href="#" class="financial-user">Don Jonny</a>
							<h4>Design a Logo for Spud on Stick for US Company </h4>
						</div>
					</div>
					<div data-title="Total $2500" class="td td-blue">
						$1100
					</div>
					<div data-title="Paid $1000" class="td td-light-blue">
						$700
					</div>
					<div data-title="{Outstanding} $1500" class="td td-dark-gray">
						20
					</div>
				</div>
				<div class="tbody">
					<div data-title="{Project_Details}" class="td">
						<div class="financial-row">
							<a href="#" class="financial-img" title="%fullName%"><img src="{SITE_IMG}user1.jpg" alt=""></a>
							<a href="#" class="financial-user">Don Jonny</a>
							<h4>Design a Logo for Spud on Stick for US Company </h4>
						</div>
					</div>
					<div data-title="Total $2500" class="td td-blue">
						$1100
					</div>
					<div data-title="Paid $1000" class="td td-light-blue">
						$700
					</div>
					<div data-title="{Outstanding} $1500" class="td td-dark-gray">
						20
					</div>
				</div>
				<div class="tbody">
					<div data-title="{Project_Details}" class="td">
						<div class="financial-row">
							<a href="#" class="financial-img" title="%fullName%"><img src="{SITE_IMG}user1.jpg" alt=""></a>
							<a href="#" class="financial-user">Don Jonny</a>
							<h4>Design a Logo for Spud on Stick for US Company </h4>
						</div>
					</div>
					<div data-title="Total $2500" class="td td-blue">
						$1100
					</div>
					<div data-title="Paid $1000" class="td td-light-blue">
						$700
					</div>
					<div data-title="{Outstanding} $1500" class="td td-dark-gray">
						20
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="no_data_msg" style="    width: 80%;
position: absolute;
top: 45%;
margin-left: 78px;
padding: 10px;">
	{There_are_no_projects} %status% {right_now}.
</div>