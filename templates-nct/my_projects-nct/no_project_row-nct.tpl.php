<section class="no_rec_section">
	<div class="section-middle-right-project" >
		<span class="profileimage">
			<img class="img-circle" src="{SITE_IMG}profileimage.jpg"> <a href="#">Don Jonny</a>
		</span>
		<h4><a href="#">Design a Logo for Spud on Stick for US Company</a>
		<button type="button" class="btn btn-primary small-btn featured-btn">
			Featured
		</button></h4>
		<p>
			{adhoc_credit_text} It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
		</p>
		<ul>
			<li>
				{Est_Budget}<b>$10</b>
			</li>
			<li>
				Bids<b>$10</b>
			</li>
		</ul>
	</div>
	<div class="clearfix"></div>
	<div class="section-middle-right-project" >
        <span class="profileimage">
            <img class="img-circle" src="{SITE_IMG}profileimage.jpg"> <a href="#">Don Jonny</a>
        </span>
        <h4><a href="#">Design a Logo for Spud on Stick for US Company</a>
        <button type="button" class="btn btn-primary small-btn featured-btn">
            Featured
        </button></h4>
        <p>
            {adhoc_credit_text} It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
        </p>
        <ul>
            <li>
                {Est_Budget}<b>$10</b>
            </li>
            <li>
                Bids<b>$10</b>
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
    
	<div class="no_data_msg">
		<p>
			{This_user_does_not_have_any} %status% projects
		</p>
	</div>
</section>
