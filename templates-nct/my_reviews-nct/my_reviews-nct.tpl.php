<section class="mainpart">
	<!-- /Section-heading start -->
	<section class="section-heading">
		<div class="container">
			<h1>{My_Reviews} (%totalReviews%)</h1>
		</div>
	</section>
	<!-- /Section-heading over -->
	<!-- /Section-middle start -->
	<section class="reviews-ratings ">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="clearfix"></div>
					<div class="search-right">
						<ul class="search-row" data-infinite="container">
							%rows%
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</section>
	<!-- /Section-middle over -->
</section>
