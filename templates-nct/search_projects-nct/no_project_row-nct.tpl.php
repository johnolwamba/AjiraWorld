<section class="no_rec_section">
	<li class="search-cell">
		<div class="search-left">
			<a href="#" class="search-user">
			<span class="user-photo">
				<img src="{SITE_IMG}user1.jpg" alt="">
			</span> Don Jonny </a>
		</div>
		<div class="search-right-content">
			<h3>Design a Logo for Spud on Stick for US Company <a href="#" class="fa fa-heart-o like-icon"></a></h3>
			<p class="search-disc">
				We are looking for an amazing logo to match our amazing company service/product. Spud on Stick will be selling spiral potato's on a stick at festivals and markets. Our tent is yellow and white striped so that will get...
			</p>
			<ul class="search-tag">
				<li>
					<a href="#" class="btn tag-btn">Photoshop</a>
				</li>
				<li>
					<a href="#" class="btn tag-btn">Illustration</a>
				</li>
			</ul>
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-sm-3">
					<p class="search-bottom">
						{Est_Budget} <strong>$700</strong>
					</p>
				</div>
				<div class="col-sm-4">
					<div class="search-bottom">
						Bids <strong><a href="#" class="tooltip-top"> 10
						<div class="tooltip_box">
							<ul class="tooltip-row">
								<li class="left-cell">
									{Average_Value}
								</li>
								<li class="right-cell">
									<strong>$1000</strong>
								</li>
								<li class="left-cell">
									{Average_ETA}
								</li>
								<li class="right-cell">
									<strong>10 days</strong>
								</li>
							</ul>
						</div> </a> </strong>
					</div>
				</div>
				<div class="col-sm-2">
					<p class="search-bottom">
						<i>2-Jun-2016</i>
					</p>
				</div>
				<div class="col-sm-3">
					<span class="feature-label">
						{Featured}
					</span>
				</div>
			</div>
		</div>
	</li>
	<div class="no_data_msg">
        <p>
            {There_are_no_projects_meeting_your_search_criteria}
        </p>
    </div>
</section>