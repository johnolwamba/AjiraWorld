<section class="mainpart">
	<!-- /Section-heading start -->
	<section class="section-heading">
		<div class="container">
			<h1>{Contact_us}</h1>
		</div>
	</section>
	<section class="contact-main">
		<div class="container">
			<div class="contact-form">
				<div class="white-box">
					<form id="contactForm" name="contactForm">
<input type="hidden" name="token" value="%tokenValue%">
						<h2><i class="fa fa-comment-o"></i> {contact_us_inner_title}</h2>
						<div class="form-group">
							<input class="form-control" placeholder="{First_Name}" type="text"
							name="firstName"
							value="%firstName%"
							data-validation="length alphanumeric"
							data-validation-length="2-25"
							data-validation-error-msg="{err_First_name_has_to_be_an_alphanumeric_value}">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="{Last_Name}" type="text"
							name="lastName"
							value="%lastName%"
							data-validation="length alphanumeric"
							data-validation-length="2-25"
							data-validation-error-msg="{err_Last_name_has_to_be_an_alphanumeric_value}">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="{Email}" type="email"
							value="%email%"
							name="email"
							data-validation="required email">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" placeholder="{Contact_No}" tabindex="4"
							value=""
							data-validation="length number"
							data-validation-length="10-13"
							name="contactNo">
						</div>
						<div class="form-group">
							<textarea class="form-control" name="message" rows="4" placeholder="Message"
							data-validation="required"                            
                            data-validation-error-msg="{err_Please_type_your_message}"></textarea>
						</div>
						<input type="hidden" name="action" value="method"/>
						<input type="hidden" name="method" value="submitContactForm"/>
						<button type="submit" class="btn btn_blue btn-block" data-ele="submitContactForm" name="submitContactForm">
							<strong>{Submit}</strong>
						</button>
					</form>
				</div>
			</div>
		</div>
	</section>
</section>
