<section>
	<div class="chose_user_type">
		<div class="container">
			<h2 class="font_light text-center">{Choose_User_Type}</h2>
			<div class="row">
				<form id="formUserType" method="post">
				<input type="hidden" name="token" value="%tokenValue%">
					<div class="col-sm-5">
						<div class="want_to center">
							<img src="{SITE_IMG}customer.png" alt="customer">
							<h4 class="font_light marginbtm20 margintop20">I want to hire a freelancer</h4>
							<p class="font-size16 marginbtm0">
								{Find_collaborate_with}
							</p>
							<p class="font-size16">
								{and_pay_an_expert}
							</p>
							<input type="submit" name="userType" class="btn btn_blue margintop30 btn_light_hover" value="Customer">
						</div>
					</div>
					<div  class="col-sm-2 want_to center hidden-xs">
						<img src="{SITE_IMG}or.png" alt="or">
					</div>
					<div class="col-sm-5">
						<div class="want_to center">
							<img src="{SITE_IMG}provider.png" alt="provider">
							<h4 class="font_light marginbtm20 margintop20">{I_want_to_work_online}</h4>
							<p class="font-size16 marginbtm0">
								{Find_collaborate_with}
							</p>
							<p class="font-size16">
								{and_pay_an_expert}
							</p>
							<input type="submit" name="userType" class="btn btn_blue btn_dark margintop30 btn_dark_hover" value="Provider">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
