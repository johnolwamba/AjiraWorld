<?php
 
ob_start();
session_name('NCT');
session_start();
set_time_limit(0);

session_set_cookie_params(3600);

date_default_timezone_set('Asia/Kolkata');

global $db, $helper, $fields, $module, $adminUserId, $sessUserId, $objHome, $main_temp, $breadcrumb, $Permission, $memberId;
global $head, $header, $left, $right, $footer, $content, $title, $resend_email_verification_popup;
global $css_array, $js_array, $js_variables;
global $dataOnly,$exchangeRates;;

$include_sharing_js = false;

$header_panel = true;
$footer_panel = true;
$styles       = array();
$scripts      = array();

$reqAuth = isset($reqAuth) ? $reqAuth : false;

$allowedUserType = isset($allowedUserType) ? $allowedUserType : 'a';

$adminUserId = (isset($_SESSION["adminUserId"]) && $_SESSION["adminUserId"] > 0 ? (int) $_SESSION["adminUserId"] : 0);

$sessUserId      = (isset($_SESSION["userId"]) && (int) $_SESSION["userId"] > 0 ? (int) $_SESSION["userId"] : 0);
$sessFirstName   = (isset($_SESSION["firstName"]) && $_SESSION["firstName"] != '' ? $_SESSION["firstName"] : null);
$sessLastName    = (isset($_SESSION["lastName"]) && $_SESSION["lastName"] != '' ? $_SESSION["lastName"] : null);
$sessUserType    = (isset($_SESSION["userType"]) && $_SESSION["userType"] != '' ? $_SESSION["userType"] : null);
$sessUserName    = (isset($_SESSION["userName"]) && $_SESSION["userName"] != '' ? $_SESSION["userName"] : null);
$sessProfileLink = (isset($_SESSION["profileLink "]) && $_SESSION["profileLink "] != '' ? $_SESSION["profileLink "] : null);

$toastr_message = isset($_SESSION["toastr_message"]) ? $_SESSION["toastr_message"] : null;
unset($_SESSION['toastr_message']);

$memberId = isset($sessUserId) ? $sessUserId : 0;

require_once 'database-nct.php';

require_once 'functions-nct/class.pdohelper.php';
require_once 'functions-nct/class.pdowrapper.php';
require_once 'functions-nct/class.pdowrapper-child.php';
require_once 'mime_type_lib.php';

$dbConfig = array(
    "host"     => DB_HOST,
    "dbname"   => DB_NAME,
    "username" => DB_USER,
    "password" => DB_PASS,
);
$db     = new PdoWrapper($dbConfig);
$helper = new PDOHelper();
if (ENVIRONMENT == 'p') {
    $db->setErrorLog(false);
} else {
    $db->setErrorLog(true);
}
require_once 'constant-nct.php';
require_once 'functions-nct/functions-nct.php';
/*register_shutdown_function("fatal_handler");*/

require_once DIR_FUN . 'validation.class.php';

require_once DIR_INC . 'FirePHPCore/FirePHP.class.php';
global $fb;
$fb = FirePHP::getInstance(true);

curPageURL();
curPageName();

checkIfIsActive();
Authentication($reqAuth, true, $allowedUserType);

require "class.main_template-nct.php";

$main    = new MainTemplater();
$msgType = isset($_SESSION["msgType"]) ? $_SESSION["msgType"] : null;
unset($_SESSION['msgType']);

if (domain_details('dir') == 'admin-nct') {
    $left_panel = true;
    require_once DIR_ADM_INC . 'functions-nct/admin-function-nct.php';
    require_once DIR_ADM_MOD . 'home-nct/class.home-nct.php';
    $objHome = new Home($module, 0);
    //start:: for multi currency
    if (!isset($_SESSION['exchangeRates'])) {
        $exchangeRates         = array();
        $exchangeRates['base'] = DEFAULT_CURRENCY_CODE;
        $results               = $db->select('tbl_currencies', '*')->results();
        foreach ($results as $key => $value) {
            $exchangeRates['rate_to_usd'][$value['currencyCode']] = $value['rate_to_usd'];
        }
        $_SESSION['exchangeRates'] = $exchangeRates;
    } else {
        $exchangeRates = $_SESSION['exchangeRates'];
    }

} else {

//start:: for multi-language

    if (isset($_SESSION["lId"])) {
        $doesExist = getTableValue("tbl_languages", "id", array("id" => $_SESSION["lId"], 'status' => 'a'));        
        if (!$doesExist) {
            unset($_SESSION["lId"]);
        }
    } else {
        $_SESSION["lId"] = getTableValue("tbl_languages", "id", array("isDefault" => 'y'));
    }

    if (isset($_SESSION["lId"]) && file_exists(DIR_INC . 'languages/' . $_SESSION["lId"] . '.php')) {
        require_once DIR_INC . 'languages/' . $_SESSION["lId"] . '.php';
        require_once DIR_INC . 'languages/1.php';
    } else {
        $_SESSION["lId"] = getTableValue("tbl_languages", "id", array("isDefault" => 'y'));
        require_once DIR_INC . 'languages/' . $_SESSION["lId"] . '.php';
    }
    $_SESSION['langName'] = getTableValue("tbl_languages", "languageName", array("id" => $_SESSION['lId']));
//end:: for multi-language

    $userCurrencyQry = "SELECT u.currencyId, c.* FROM tbl_users AS u INNER JOIN tbl_currencies AS c ON c.id = u.currencyId WHERE u.userId = '".$sessUserId."' AND c.isActive = 'y' ";
    $userCurrency = $db->pdoQuery($userCurrencyQry)->affectedRows();

    if($userCurrency > 0){
        $userCurrencyDetails = $db->pdoQuery($userCurrencyQry)->result();
        $_SESSION['sessCurrencyCode'] = $userCurrencyDetails['currencyCode'];
        $_SESSION['sessCurrencySign'] = $userCurrencyDetails['currencySign'];
        $_SESSION['sessCurrencyRate'] = $userCurrencyDetails['rate_to_usd'];
    } else {
        $_SESSION['sessCurrencyCode'] = 'USD';
        $_SESSION['sessCurrencySign'] = '$';
        $_SESSION['sessCurrencyRate'] = '1.00';
    }

    //echo $_SESSION['sessCurrencyCode'];

//start:: for multi currency
    if (!isset($_SESSION['exchangeRates'])) {
        $exchangeRates         = array();
        $exchangeRates['base'] = DEFAULT_CURRENCY_CODE;
        $results               = $db->select('tbl_currencies', '*')->results();
        foreach ($results as $key => $value) {
            $exchangeRates['rate_to_usd'][$value['currencyCode']] = $value['rate_to_usd'];
        }
        $_SESSION['exchangeRates'] = $exchangeRates;
    } else {
        $exchangeRates = $_SESSION['exchangeRates'];
    }
//end:: for multi currency

    require_once DIR_MOD . 'home-nct/class.home-nct.php';
    require_once DIR_INC . "paypal-nct/paypal_class.php";
    $objHome = new Home("home-nct");
}

$objPost = new stdClass();

$description = SITE_NM;
$keywords    = "";
