<?php
class Profile {
	function __construct($module = "", $id = 0, $reqData = array()) {
		global $js_variables;
		foreach ($GLOBALS as $key => $values) {
			$this -> $key = $values;
		}
		$this -> module = $module;
		$this -> id = $id;
		$this -> reqData = $reqData;
		$this -> dataOnly = (isset($this->reqData['dataOnly']) && $this->reqData['dataOnly']==true)?true:false;

		$this->sessUserId = (isset($reqData['userId']) && $reqData['userId'] > 0) ? $reqData['userId'] : $this->sessUserId;
		
		$this -> table = "tbl_users";

		$this -> user = $this -> userData($reqData['profileLink']);
		$this -> isMe = ($this -> user['userId'] == $this -> sessUserId) ? 1 : 0;
		$this -> isDifferent = ($this -> user['userType'] != $this -> sessUserType) ? 1 : 0;		
		$js_variables = ($this->user['userType'] == "p")?"var method ='reviews_rows'":"var method ='project_rows'";
		
	}

	public function reviews_rows() {
		global $db;
		$html = $qry = $condition = $sort_by = null;
		$paramArray = array($this -> user['userId']);
		$qry = "SELECT IFNULL(f.averageRating, 0) AS averageRating, f.`review`, f.`addedDate`, CONCAT_WS(' ',u.`firstName`,u.`lastName`) AS fullName, u.`profilePhoto`, u.`profileLink` FROM tbl_feedbacks AS f LEFT JOIN tbl_users AS u ON f.`userFrom` = u.userId where f.userTo=? AND u.isActive='y'";

		////////////////////////////
		// put pagination
		$limit_cond = NULL;

		$q = $this -> db -> pdoQuery($qry . $condition . $sort_by, $paramArray);
		$totalRows = $q -> affectedRows();
		$pageNo = isset($this -> reqData["pageNo"]) ? $this -> reqData["pageNo"] : 0;
		$pager = getPagerData($totalRows, LIMIT, $pageNo);
		//dump($pager);
		//dump($pageNo);
		if ($pageNo <= $pager -> numPages) {
			$offset = $pager -> offset;
			if ($offset < 0) {
				$offset = 0;
			}

			$limit = $pager -> limit;

			$page = $pager -> page;
			$limit_cond = " LIMIT $offset, $limit";
			$qry = $this -> db -> pdoQuery($qry . $condition . $sort_by . $limit_cond, $paramArray);

			if($this->dataOnly)
			{
				return $qry->results();
			}

		}
		////////////////////////////

		if ($qry -> affectedRows() > 0) {
			$results = $qry -> results();
			foreach ($results as $k => $v) {
				extract($v);
				$replace = array(
					'%averageRating%' => renderStarRating($averageRating),
					'%review%' => filtering(nl2br($review)),
					'%addedDate%' => date("d-M-Y", strtotime($addedDate)),
					'%fullName%' => ucwords(filtering($fullName)),
					'%profileLink%' => filtering($profileLink),
					'%profilePhoto%' => tim_thumb_image($profilePhoto, 'profile', 100, 100),
				);
				$html .= get_view(DIR_TMPL . $this -> module . "/reviews_and_ratings_row-nct.tpl.php", $replace);
			}
		}
		else {
			$html .= get_view(DIR_TMPL . $this -> module . "/no_reviews_and_ratings_row-nct.tpl.php", array('%text%' => This_user_has_not_received_any_reviews_yet));
		}

		return $html;
	}

	public function project_rows($project_status = 'open') {
		global $db;
		$html = $qry = $condition = $sort_by = null;
		$paramArray = array($this -> user['userId']);

		$project_status = (isset($this -> reqData['status'])) ? $this -> reqData['status'] : $project_status;
		switch ($project_status) {
			case 'open' :
				if ($this -> user['userType'] == 'c') {
					$qry = "SELECT p.id, p.title, COUNT(b.id) AS bids, p.slug, p.`description`, p.budget AS budget, p.`isFeatured`, p.`slug`, u.`profileLink`, u.`profilePhoto`, CONCAT_WS(' ', u.firstName, u.lastName) AS fullName FROM tbl_projects AS p LEFT JOIN tbl_users AS u ON p.userId = u.userId LEFT JOIN tbl_bids AS b ON p.`id` = b.`projectId` WHERE p.`isActive` = 'y' AND u.`isActive` = 'y' AND p.`jobStatus` = 'open' AND p.userId=? GROUP BY p.id ORDER BY p.`id` DESC  ";					
				}
				else {
					$qry = "SELECT p.id, p.title, COUNT(b.id) AS bids, p.slug, p.`description`, p.budget AS budget, p.`isFeatured`, p.`slug`, u.`profileLink`, u.`profilePhoto`, CONCAT_WS(' ', u.firstName, u.lastName) AS fullName FROM tbl_projects AS p LEFT JOIN tbl_users AS u ON p.providerId = u.userId LEFT JOIN tbl_bids AS b ON p.`id` = b.`projectId` WHERE p.`isActive` = 'y' AND u.`isActive` = 'y' AND p.`jobStatus` = 'open' AND p.providerId=? GROUP BY p.id ORDER BY p.`id` DESC  ";					
				}
				$est_or_total = 'Est.';
				break;
			case 'completed' :
				if ($this -> user['userType'] == 'c') {
					$qry = "SELECT p.id, p.title, COUNT(b.id) AS bids, p.slug, p.`description`, p.price AS budget, p.`isFeatured`, p.`slug`, u.`profileLink`, u.`profilePhoto`, CONCAT_WS(' ', u.firstName, u.lastName) AS fullName FROM tbl_projects AS p LEFT JOIN tbl_users AS u ON p.userId = u.userId LEFT JOIN tbl_bids AS b ON p.`id` = b.`projectId` WHERE p.`isActive` = 'y' AND u.`isActive` = 'y' AND p.`jobStatus` = 'completed' AND p.userId=? GROUP BY p.id ORDER BY p.`id` DESC  ";			
				}
				else {
					$qry = "SELECT p.id, p.title, COUNT(b.id) AS bids, p.slug, p.`description`, p.price AS budget, p.`isFeatured`, p.`slug`, u.`profileLink`, u.`profilePhoto`, CONCAT_WS(' ', u.firstName, u.lastName) AS fullName FROM tbl_projects AS p LEFT JOIN tbl_users AS u ON p.providerId = u.userId LEFT JOIN tbl_bids AS b ON p.`id` = b.`projectId` WHERE p.`isActive` = 'y' AND u.`isActive` = 'y' AND p.`jobStatus` = 'completed' AND p.providerId=? GROUP BY p.id ORDER BY p.`id` DESC  ";					
				}
				$est_or_total = 'Total';
				break;
		}

		////////////////////////////
		// put pagination
		$limit_cond = NULL;

		$q = $this -> db -> pdoQuery($qry . $condition . $sort_by, $paramArray);
		$totalRows = $q -> affectedRows();
		$pageNo = isset($this -> reqData["pageNo"]) ? $this -> reqData["pageNo"] : 0;
		$pager = getPagerData($totalRows, LIMIT, $pageNo);
		//dump($pager);
		//dump($pageNo);
		if ($pageNo <= $pager -> numPages) {
			$offset = $pager -> offset;
			if ($offset < 0) {
				$offset = 0;
			}

			$limit = $pager -> limit;

			$page = $pager -> page;
			$limit_cond = " LIMIT $offset, $limit";
			$qry = $this -> db -> pdoQuery($qry . $condition . $sort_by . $limit_cond, $paramArray);

		}
		////////////////////////////

		if ($qry -> affectedRows() > 0) {
			$results = $qry -> results();
			foreach ($results as $k => $v) {
				$replace = array(
					'%profilePhoto%' => tim_thumb_image($v['profilePhoto'], 'profile', 75, 75),
					'%profileLink%' => filtering($v['profileLink']),
					'%fullName%' => ucwords(filtering($v['fullName'])),
					'%slug%' => $v['profileLink']."/".$v['slug'],
					'%title%' => ucfirst(filtering($v['title'])),
					'%isFeatured%' => ($v['isFeatured'] == 'n') ? 'hide' : null,
					'%desc%' => filtering(String_crop($v['description'], 230)),
					'%est_or_total%' => $est_or_total,
					'%budget%' => CURRENCY_SYMBOL . filtering($v['budget']),
					'%bids%' => filtering($v['bids']),
					'%pid%' => $v['id'],
					'%like_icon%' => is_my_fav($v['id'], 'project') ? 'fa-heart' : 'fa-heart-o'
				);
				if ($this -> isDifferent) {
					$html .= get_view(DIR_TMPL . $this -> module . "/project_row_with_heart-nct.tpl.php", $replace);
				}
				else {
					$html .= get_view(DIR_TMPL . $this -> module . "/project_row-nct.tpl.php", $replace);
				}

			}
		}
		else {
			$html .= get_view(DIR_TMPL . $this -> module . "/no_project_row-nct.tpl.php", array('%status%' => constant(ucwords($project_status))));
		}

		return $html;
	}

	public function get_contact() {
		$replace = array(
			'%email%' => filtering($this -> user['email']),
			'%contact_no%' => filtering("+" . $this -> user['contactCode'] . " - " . $this -> user['contactNo']),
		);
		return get_view(DIR_TMPL . $this -> module . "/contact-nct.tpl.php", $replace);
	}

	public function get_review() {
		$results = $this -> db -> pdoQuery("SELECT AVG(f.averageRating) AS average, count(f.id) AS total FROM tbl_feedbacks AS f LEFT JOIN tbl_users AS u ON f.`userfrom` = u.userid WHERE f.userto = ? AND u.isactive = 'y' ", array('userTo' => $this -> user['userId'])) -> result();
		$replace = array(
			'%stars%' => renderStarRating($results['average']),
			'%total%' => $results['total']
		);

		if($this->dataOnly)
		{
			return array('total_review'=>$results['total'],'average'=>$results['average']);
		}
		return get_view(DIR_TMPL . $this -> module . "/review-nct.tpl.php", $replace);
	}

	public function total_money($userId, $userType = NULL) {
		global $db;
		if ($userType == 'c') {
			//total spent
            $total = $db -> pdoQuery("SELECT IFNULL(SUM(ph.totalAmount), 0) AS totalAmount FROM tbl_payment_history AS ph WHERE ph.`userId` = ? AND (ph.paymentType = 'featured' OR ph.paymentType = 'project payment' )", array($userId)) -> result();
		}
		elseif ($userType == 'p') {
			 //total earned
            $total = $db -> pdoQuery("SELECT IFNULL(SUM(m.price), 0) AS totalAmount FROM tbl_milestone AS m INNER JOIN tbl_projects AS p ON m.projectId = p.id WHERE p.providerId = ? AND m.status = 'paid' ", array($userId)) -> result();
		}
		else {
			return "0.00";
		}

		return abs($total['totalAmount']);
	}

	public function getPageContent() {
		$user = $this -> user;
		// note... if both profile type are different then hide contact details else show them
        
		$replace = array(
			//top bar
			'%profilePhoto%' => tim_thumb_image($user['profilePhoto'], 'profile', 300, 300),
			'%level_hide%' => ($user['userType'] == 'p') ? null : 'hide',
			'%level%' => ($user['userType'] == 'p') ?  ucwords($user['experience']) : null,
			'%profileLink%' => $user['profileLink'],
			'%userId%' => $user['userId'],
			'%edit_icon%' => ($this -> isMe) ? null : 'hide',
			'%heart_hide%' => (($this -> isMe || $user['userType'] == 'c') || $this->sessUserType == 'p') ? 'hide':null,
			'%heart_icon%' => ($user['isFavorite']) ? 'fa-heart':'fa-heart-o',
			'%fullName%' => ucwords($user['fullName']),
			'%userType%' => ($user['userType'] == 'p') ? 'Provider' : 'Customer',
			'%earned_or_spent_number%' => $_SESSION['sessCurrencySign'].exchange($this->total_money($user['userId'], $user['userType']), $from = 'USD', $to = $_SESSION['sessCurrencyCode']),
			'%earned_or_spent_text%' => ($user['userType'] == 'p') ? 'Earned' : 'Spent',
			'%comp_proj%' => $user['completed'],
			'%ongoing_proj%' => $user['ongoing'],
			'%review%' => ($user['userType'] == 'p') ? $this -> get_review() : null,
			
			//contact
			'%contact%' => ($this -> isDifferent && (int)$this->sessUserId >0) ? $this -> get_contact() : null,
			
			//about me
			'%about%' => (trim($user['aboutMe'])!=null)?nl2br(filtering($user['aboutMe'])):user_profile_no_about_text_message,
			
			//verifications
			'%f_check%' => ($user['facebook_verify'] == '1') ? 'fa-check-circle' : 'fa-times-circle',
			'%f_connected%' => ($user['facebook_verify'] == '1') ? Connected : null,
			'%f_link%' => ($this -> isMe && ($user['facebook_verify'] == '0' || $user['facebook_verify'] == '')) ? "<a href='javascript:void(0)' onclick='verify(\"facebook\")' class='verify-link'>Verify</a>" : null,

			'%g_check%' => ($user['google_verify'] == '1') ? 'fa-check-circle' : 'fa-times-circle',
			'%g_connected%' => ($user['google_verify'] == '1') ? Connected : null,
			'%g_link%' => ($this -> isMe && ($user['google_verify'] == '0' || $user['google_verify'] == '')) ? "<a href='javascript:void(0)' onclick='verify(\"google\")' class='verify-link'>Verify</a>" : null,

			'%l_check%' => ($user['linkedin_verify'] == '1') ? 'fa-check-circle' : 'fa-times-circle',
			'%l_connected%' => ($user['linkedin_verify'] == '1') ? Connected : null,
			'%l_link%' => ($this -> isMe && ($user['linkedin_verify'] == '0' || $user['linkedin_verify'] == '')) ? "<a href='javascript:void(0)' onclick='verify(\"linkedin\")' class='verify-link'>Verify</a>" : null,

			//project section
			'%projects_or_reviews%' => ($user['userType'] == 'p') ? $this -> reviews_section() : $this -> projects_section(),
		);

		return get_view(DIR_TMPL . $this -> module . "/" . $this -> module . ".tpl.php", $replace);
	}

	public function projects_section() {
		$replace = array('%rows%' => $this -> project_rows());
		return get_view(DIR_TMPL . $this -> module . "/project-nct.tpl.php", $replace);
	}

	public function reviews_section() {
		$replace = array('%rows%' => $this -> reviews_rows());
		return get_view(DIR_TMPL . $this -> module . "/reviews_and_ratings-nct.tpl.php", $replace);
	}

	public function userData($profileLink) {
		$doesExist = getTableValue($this -> table, 'userId', array(
			'profileLink' => $profileLink,
			'isActive' => 'y',
			'status' => 'a',
		));
		if (isset($doesExist) && $doesExist > 0) {
			$arr = array();
			$arr = $this -> db -> pdoQuery("SELECT IFNULL(f.id,0) as isFavorite, u.*, CONCAT_WS(' ',u.firstName,u.lastName) AS fullName FROM tbl_users AS u left join tbl_favourites as f on f.favoriteId = u.userId and f.type='1' and f.userId=? WHERE u.profileLink=? ",array($this->sessUserId,$profileLink)) -> result();
			if ($arr['userType'] == 'c') {
				$arr['ongoing'] = getTableValue('tbl_projects', 'count("id")', array(
					'userId' => $arr['userId'],
					'jobStatus' => 'progress',
					'isActive' => 'y'
				));
				$arr['completed'] = getTableValue('tbl_projects', 'count("id")', array(
					'userId' => $arr['userId'],
					'jobStatus' => 'completed',
					'isActive' => 'y'
				));
			}
			else {
				$arr['ongoing'] = getTableValue('tbl_projects', 'count("id")', array(
					'providerId' => $arr['userId'],
					'jobStatus' => 'progress',
					'isActive' => 'y'
				));
				$arr['completed'] = getTableValue('tbl_projects', 'count("id")', array(
					'providerId' => $arr['userId'],
					'jobStatus' => 'completed',
					'isActive' => 'y'
				));
			}
			$arr['earned_or_spent_amount'] = $this -> total_money($arr['userId'], $arr['userType']);
			
			return $arr;
			
		}
		else {
			if($this->dataOnly){
	            return array();
	        }else{
	        	$_SESSION["msgType"] = disMessage(array(
					'type' => 'err',
					'var' => "What you are looking for does not exist. Try exploring something interesting."
				));
				redirectPage(SITE_URL);		            
	        }
			
		}
	}

}
?>
