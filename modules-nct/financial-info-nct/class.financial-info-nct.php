<?php
class FinancialInfo
{
    public function __construct($module = "", $id = 0, $reqData = array())
    {
        foreach ($GLOBALS as $key => $values) {
            $this->$key = $values;
        }
        $this->module  = $module;
        $this->id      = $id;
        $this->reqData = $reqData;

        //for web service
        $this->dataOnly     = (isset($this->reqData['dataOnly']) && $this->reqData['dataOnly'] == true) ? true : false;
        $this->sessUserId   = (isset($reqData['userId']) && $reqData['userId'] > 0) ? $reqData['userId'] : $this->sessUserId;
        $this->sessUserType = getTableValue('tbl_users', 'userType', array('userId' => $this->sessUserId));

        $this->completed_totals = $this->completed_row(true);
        $this->progress_totals  = $this->inProgress_row(true);

    }

    public function completed_row($totals = false)
    {
        $results = array();
        $html    = null;

        if ($this->sessUserType == "c") {
            $results = $this->db->pdoQuery('SELECT p.id , p.price AS total, p.slug, p.title, u.`profileLink`, CONCAT_WS(" ", u.`firstName`, u.`lastName`) AS fullName, u.`profilePhoto` FROM `tbl_projects` AS p LEFT JOIN tbl_users AS u ON p.userId = u.`userId` LEFT JOIN (SELECT IFNULL(SUM(m.price), 0) AS paid, m.`projectId` FROM tbl_milestone AS m WHERE m.status = "paid") AS tbl_paid ON p.`id` = tbl_paid.projectId WHERE u.`isActive` = "y" AND p.userId = ? AND p.jobStatus =? ', array(
                $this->sessUserId,
                "completed",
            ))->results();
        } else {
            $results = $this->db->pdoQuery('SELECT p.`id`, @pprice:= p.price AS total, p.slug, p.title, u.`profileLink`, CONCAT_WS(" ", u.`firstname`, u.`lastname`) AS fullName, u.`profilePhoto` FROM `tbl_projects` AS p LEFT JOIN tbl_users AS u ON p.userid = u.`userid` LEFT JOIN `tbl_milestone` AS m ON p.id = m.projectId  WHERE u.`isactive` = "y" AND p.providerid = ? AND p.jobstatus = ? GROUP BY p.`id`', array(
                $this->sessUserId,
                "completed",
            ))->results();
        }

        //calculate amounts
        $total_price = $total_paid = $total_outstanding = 0;
        if (!empty($results)) {
            foreach ($results as $k => $v) {
                extract($v);
                $paidAmt        = $this->db->pdoQuery("SELECT SUM(price) AS paid FROM tbl_milestone WHERE projectId = ? AND status = ? AND isNulled = ? AND isFinal = ?", array($id, "paid", "n", "y"))->result();
                $outstandingAmt = $this->db->pdoQuery("SELECT SUM(price) AS outstanding FROM tbl_milestone WHERE projectId = ? AND (status = ? OR status = ?) AND isNulled = ? AND isFinal = ?", array($id, "unapproved", "remain", "n", "y"))->result();
                $paid           = $paidAmt['paid'];
                $outstanding    = $outstandingAmt['outstanding'];
                if ($totals == false) {
                    $html .= get_view(DIR_TMPL . $this->module . "/in_progress_row-nct.tpl.php", array(
                        '%profileLink%'       => SITE_URL . $profileLink,
                        '%projLink%'          => SITE_URL . $profileLink . '/' . $slug,
                        '%profilePhoto%'      => tim_thumb_image($profilePhoto, 'profile', 74, 74),

                        '%total_price%'       => $this->completed_totals['total_price'],
                        '%total_paid%'        => $this->completed_totals['total_paid'],
                        '%paid_or_earned%'    => ($this->sessUserType == "c") ? 'Paid' : 'Earned',
                        '%total_outstanding%' => $this->completed_totals['total_outstanding'],

                        '%fullName%'          => $fullName,
                        '%title%'             => filtering($title),
                        '%total%'             => $_SESSION['sessCurrencySign'].number_format(exchange($total, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
                        '%paid%'              => $_SESSION['sessCurrencySign'].number_format(exchange($paid, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
                        '%outstanding%'       => $_SESSION['sessCurrencySign'].number_format(exchange($outstanding, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),

                    ));
                }
                $total_price += $total;
                $total_paid += $paid;
                $total_outstanding += $outstanding;
            }

            if ($this->dataOnly) {
                return array(
                    'total_price'       => $total_price,
                    'total_paid'        => $total_paid,
                    'total_outstanding' => $total_outstanding,
                    'data'              => $results,
                );
            }
        } else {

        }
        return ($totals == true) ? array(
            'total_price'       => $_SESSION['sessCurrencySign'].number_format(exchange($total_price, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
            'total_paid'        => $_SESSION['sessCurrencySign'].number_format(exchange($total_paid, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
            'total_outstanding' => $_SESSION['sessCurrencySign'].number_format(exchange($total_outstanding, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
        ) : $html;
    }

    public function completed()
    {
        if ($this->completed_row() == null) {
            return get_view(DIR_TMPL . $this->module . "/no_in_progress_row-nct.tpl.php", array('%status%' => 'completed'));
        } else {

            extract($this->completed_row(true));
            return get_view(DIR_TMPL . $this->module . "/in_progress-nct.tpl.php", array(
                // '%total_price%'       => $_SESSION['sessCurrencySign'].number_format(exchange($total_price, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
                // '%total_paid%'        => $_SESSION['sessCurrencySign'].number_format(exchange($total_paid, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
                // '%total_outstanding%' => $_SESSION['sessCurrencySign'].number_format(exchange($total_outstanding, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
                // '%paid_or_earned%'    => ($this->sessUserType == "c") ? 'Paid' : 'Earned',
                // '%row%'               => $this->completed_row(),
                '%total_price%'       => $total_price,
                '%total_paid%'        => $total_paid,
                '%total_outstanding%' => $total_outstanding,
                '%paid_or_earned%'    => ($this->sessUserType == "c") ? 'Paid' : 'Earned',
                '%row%'               => $this->completed_row(),
            ));
        }

    }

    public function inProgress_row($totals = false)
    {
        $results = array();
        $html    = null;
        if ($this->sessUserType == "c") {
            $results = $this->db->pdoQuery('SELECT p.id,p.price AS total, p.slug, p.title, u.`profileLink`, CONCAT_WS(" ", u.`firstName`, u.`lastName`) AS fullName, u.`profilePhoto` FROM `tbl_projects` AS p LEFT JOIN tbl_users AS u ON p.userId = u.`userId` LEFT JOIN (SELECT IFNULL(SUM(m.price), 0) AS paid, m.`projectId` FROM tbl_milestone AS m WHERE m.status = "paid") AS tbl_paid ON p.`id` = tbl_paid.projectId WHERE u.`isActive` = "y" AND p.userId = ? AND p.jobStatus =? ', array(
                $this->sessUserId,
                "progress",
            ))->results();
        } else {
            $results = $this->db->pdoQuery('SELECT p.`id`, @pprice:= p.price AS total, p.slug, p.title, u.`profileLink`, CONCAT_WS(" ", u.`firstname`, u.`lastname`) AS fullName, u.`profilePhoto` FROM `tbl_projects` AS p LEFT JOIN tbl_users AS u ON p.userid = u.`userid` LEFT JOIN `tbl_milestone` AS m ON p.id = m.projectId  WHERE u.`isactive` = "y" AND p.providerid = ? AND p.jobstatus = ? GROUP BY p.`id`', array(
                $this->sessUserId,
                "progress",
            ))->results();
        }

        //calculate amounts
        $total_price = $total_paid = $total_outstanding = 0;
        if (!empty($results)) {
            foreach ($results as $k => $v) {
                extract($v);
                $paidAmt        = $this->db->pdoQuery("SELECT SUM(price) AS paid FROM tbl_milestone WHERE projectId = ? AND status = ? AND isNulled = ? AND isFinal = ?", array($id, "paid", "n", "y"))->result();
                $outstandingAmt = $this->db->pdoQuery("SELECT SUM(price) AS outstanding FROM tbl_milestone WHERE projectId = ? AND (status = ? OR status = ?) AND isNulled = ? AND isFinal = ?", array($id, "unapproved", "remain", "n", "y"))->result();
                $paid           = $paidAmt['paid'];
                $outstanding    = $outstandingAmt['outstanding'];
                if ($totals == false) {
                    $html .= get_view(DIR_TMPL . $this->module . "/in_progress_row-nct.tpl.php", array(
                        '%profileLink%'       => SITE_URL . $profileLink,
                        '%projLink%'          => SITE_URL . $profileLink . '/' . $slug,
                        '%profilePhoto%'      => tim_thumb_image($profilePhoto, 'profile', 74, 74),
                        '%fullName%'          => $fullName,

                        '%total_price%'       => $this->progress_totals['total_price'],
                        '%total_paid%'        => $this->progress_totals['total_paid'],
                        '%paid_or_earned%'    => ($this->sessUserType == "c") ? 'Paid' : 'Earned',
                        '%total_outstanding%' => $this->progress_totals['total_outstanding'],

                        '%title%'             => filtering($title),
                        '%total%'             => $_SESSION['sessCurrencySign'].number_format(exchange($total, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
                        '%paid%'              => $_SESSION['sessCurrencySign'].number_format(exchange($paid, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
                        '%outstanding%'       => $_SESSION['sessCurrencySign'].number_format(exchange($outstanding, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
                    ));
                }
                $total_price += $total;
                $total_paid += $paid;
                $total_outstanding += $outstanding;
            }

            if ($this->dataOnly) {
                return array(
                    'total_price'       => $total_price,
                    'total_paid'        => $total_paid,
                    'total_outstanding' => $total_outstanding,
                    'data'              => $results,
                );
            }
        } else {

        }
        return ($totals == true) ? array(
            'total_price'       => $_SESSION['sessCurrencySign'].number_format(exchange($total_price, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
            'total_paid'        => $_SESSION['sessCurrencySign'].number_format(exchange($total_paid, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
            'total_outstanding' => $_SESSION['sessCurrencySign'].number_format(exchange($total_outstanding, $from = 'USD', $to = $_SESSION['sessCurrencyCode']),2),
        ) : $html;
    }

    public function inProgress()
    {
        if ($this->inProgress_row() == null) {
            return get_view(DIR_TMPL . $this->module . "/no_in_progress_row-nct.tpl.php", array('%status%' => 'in progress'));
        } else {

            extract($this->inProgress_row(true));
            return get_view(DIR_TMPL . $this->module . "/in_progress-nct.tpl.php", array(
                '%total_price%'       => $total_price,
                '%total_paid%'        => $total_paid,
                '%total_outstanding%' => $total_outstanding,
                '%paid_or_earned%'    => ($this->sessUserType == "c") ? 'Paid' : 'Earned',
                '%row%'               => $this->inProgress_row(),
            ));
        }

    }

    public function getPageContent()
    {
        return get_view(DIR_TMPL . $this->module . "/" . $this->module . ".tpl.php", array('%tab_panel%' => $this->inProgress()));
    }

}
