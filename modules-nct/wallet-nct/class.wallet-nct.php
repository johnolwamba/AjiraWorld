<?php

class Wallet
{
    public function __construct($module = "", $id = 0, $reqData = array())
    {
        global $js_variables;
        foreach ($GLOBALS as $key => $values) {
            $this->$key = $values;
        }
        $this->module = $module;
        $this->id = $id;
        $this->reqData = $reqData;

        //for web service
        $this->dataOnly = (isset($this->reqData['dataOnly']) && $this->reqData['dataOnly'] == true) ? true : false;
        $this->sessUserId = (isset($reqData['userId']) && $reqData['userId'] > 0) ? $reqData['userId'] : $this->sessUserId;

        $this->walletAmount = getTableValue('tbl_users', 'walletamount', array('userId' => $this->sessUserId));
        $js_variables = 'toCurr="' . $_SESSION['sessCurrencyCode'] . '";';
    }

    public function submitDepositFundsForm($gateway)
    {
        extract($this->reqData);
        if ($amount < 1) {
            $response['status'] = 0;
            $response['msg'] = err_Please_enter_valid_amount;
            echo json_encode($response);
            exit;
        }

        /*$paypalId = getTableValue('tbl_users', 'paypalEmail', array('userId' => $this->sessUserId));
                    if ($paypalId == null) {
                    $response['status'] = 0;
                    $response['msg']    = Please_share_your_Paypal_id_to_proceed . ' <a href="' . SITE_EDIT_PROFILE . '">' . Click_to_edit_it_now . '</a>';
                    echo json_encode($response);
                    exit;
        */
        //make entry for pending status
        $payment_history_id = $this->db->insert('tbl_payment_history', array(
            'userId' => $this->sessUserId,
            'paymentType' => 'deposit to wallet',
            'membershipId' => 0,
            'totalAmount' => $amount,
            'ipAddress' => get_ip_address(),
            'balanceAdded' => $amount,
            'gateway' => $gateway,
            'createdDate' => date('Y-m-d H:i:s'),
        ))->lastInsertId();

        if ($gateway == 'pa') {
            // Paypal
            $en_action = base64_encode('deposit');
            $en_action_cancel = '/' . base64_encode($payment_history_id);
            $url_paypal = PAYPAL_URL;
            $url_paypal .= "?business=" . urlencode(PAYPAL_EMAIL);
            $url_paypal .= "&cmd=" . urlencode('_xclick');
            $url_paypal .= "&item_name=Deposit to wallet - " . urlencode(SITE_NM);
            $url_paypal .= "&item_number=" . urlencode($payment_history_id);
            $url_paypal .= "&custom=" . urlencode($payment_history_id . '__' . $this->sessUserId);
            $url_paypal .= "&amount=" . urlencode($amount);
            $url_paypal .= "&currency_code=" . urlencode(PAYPAL_CURRENCY_CODE);
            $url_paypal .= "&handling=" . urlencode('0');
            $url_paypal .= "&bn=" . urlencode('NCryptedTechnologies_SP_EC');
            $url_paypal .= "&return=" . urlencode(get_link('paypal_thankyou', $en_action));
            $url_paypal .= "&cancel_return=" . urlencode(get_link('paypal_failed', $en_action . $en_action_cancel));
            $url_paypal .= "&notify_url=" . urlencode(get_link('paypal_notify', $en_action));
            redirectPage($url_paypal);
            die();
        } else if ($gateway == 'pe') {
            $type = base64_encode('deposit');

            $Exstring = 'dw,' . $this->sessUserId . ',' . $payment_history_id . ',' . $amount;

            $frameString = pesaPay($this->sessUserId, $amount, get_link('pespal_thankyou', $type), base64_encode($Exstring));
            redirectPage($frameString);
            die();
        } else if ($gateway == 'mpesa') {
            //Call payfi api here


            $type = base64_encode('deposit');

            $Exstring = 'dw,' . $this->sessUserId . ',' . $payment_history_id . ',' . $amount;

            $frameString = pesaPay($this->sessUserId, $amount, get_link('pespal_thankyou', $type), base64_encode($Exstring));
            redirectPage($frameString);
            die();
        }

    }

    public function depositFundModal()
    {
        $mailPay = $this->db->select('tbl_users', array('paypalEmail', 'pesapalEmail'), array('userId' => $this->sessUserId))->result();
        $paycheck = $mailPay['paypalEmail'] != '' ? 'checked' : '';
        $pescheck = $mailPay['paypalEmail'] == '' ? 'checked' : '';

        $replace = array(
            '%PAYCHECKED%' => $paycheck,
            '%PESACHECKED%' => $pescheck
        );

        return get_view(DIR_TMPL . $this->module . "/depositFundModal-nct.tpl.php", $replace);

    }

    public function submitReedeemForm()
    {
///////////////////
        if (!isset($this->dataOnly) || !$this->dataOnly) {
            if (!checkFormToken($this->reqData['token'])) {
                $response['status'] = 0;
                $response['msg'] = "Invalid request.";
                $response['newToken'] = setFormToken();
                echo json_encode($response);
                exit;
            }
        }
///////////////////

        $walletamount = getTableValue('tbl_users', 'walletamount', array('userId' => $this->sessUserId));

        if ($walletamount > 0) {
            extract($this->reqData);
            if ($amount < 1 || $amount > $walletamount) {
                $response['status'] = 0;
                $response['msg'] = Please_enter_valid_redemption_amount;
                if ($this->dataOnly) {
                    return $response;
                    exit;
                } else {
                    echo json_encode($response);
                    exit;
                }
            }
            if (trim($description) == null) {
                $response['status'] = 0;
                $response['msg'] = Please_enter_reason_for_this_redemption;
                if ($this->dataOnly) {
                    return $response;
                    exit;
                } else {
                    echo json_encode($response);
                    exit;
                }
            }
            $this->db->delete('tbl_redeem_requests', array('userId' => $this->sessUserId, 'paymentStatus' => 'pending'));

            $insArr = array(
                'userId' => $this->sessUserId,
                'amount' => (string)$amount,
                'description' => $description,
                'paymentStatus' => 'pending',
                'createdDate' => date('Y-m-d H:i:s'),
                'updatedDate' => date('Y-m-d H:i:s'),
                'redeemedAmount' => 0,
                'paypalId' => $paypalId,
                'gateway' => isset($gateway) ? $gateway : '',
                'safaricomContactCode' => isset($safaricomContactCode) ? $safaricomContactCode : '',
                'safaricomContactNo' => isset($safaricomContactNo) ? $safaricomContactNo : ''


            );

            $lastid = $this->db->insert('tbl_redeem_requests', $insArr)->getLastInsertId();
            if ($lastid > 0) {
                $response['status'] = 1;
                $response['msg'] = Your_request_has_been_submitted_successfully;
            } else {
                $response['status'] = 0;
                $response['msg'] = toastr_something_went_wrong;
            }
            if ($this->dataOnly) {
                return $response;
                exit;
            } else {
                echo json_encode($response);
                exit;
            }
        } else {
            $response['status'] = 0;
            $response['msg'] = You_do_not_have_enough_wallet_amount_to_redeem;
            if ($this->dataOnly) {
                return $response;
                exit;
            } else {
                echo json_encode($response);
                exit;
            }

        }
    }

    public function balance_tab()
    {
        $totalCredits = getTableValue('tbl_users', 'totalCredits', array('userId' => $this->sessUserId));

        if ($this->sessUserType == 'c') {
            $result = $this->db->pdoQuery("SELECT a.available, p.pending, r.requested FROM (SELECT IFNULL(u.`walletamount`, 0) AS available FROM tbl_users AS u WHERE u.userId = ?) AS a, (SELECT IFNULL(SUM(pnd.price), 0) AS pending FROM (SELECT DISTINCT m.id,m.`price` AS price ,m.projectId,m.milestone_date FROM tbl_milestone AS m INNER JOIN tbl_projects AS tp ON m.`projectId` = tp.`id` INNER JOIN tbl_bids AS b on b.projectId = m.projectId INNER JOIN tbl_payment_history AS ph ON ph.milestoneId = m.id WHERE tp.userId = ? AND b.escrow = 'y' AND tp.jobStatus = 'progress' AND m.status != 'paid' AND m.isNulled = 'n' AND m.isFinal = 'y') AS pnd ) AS p, (SELECT IFNULL(SUM(rr.`amount`), 0) AS requested FROM tbl_redeem_requests AS rr WHERE rr.userId = ? AND (rr.`paymentStatus`='pending' OR rr.`paymentStatus`='initiated')) AS r ", array(
                $this->sessUserId,
                $this->sessUserId,
                $this->sessUserId,
            ));
        } else {
            $result = $this->db->pdoQuery("SELECT a.available, p.pending, r.requested FROM (SELECT IFNULL(u.`walletamount`, 0) AS available FROM tbl_users AS u WHERE u.userId = ?) AS a, (SELECT IFNULL(SUM(m.`price`), 0) AS pending FROM tbl_milestone AS m LEFT JOIN tbl_projects AS tp ON m.`projectId` = tp.`id` WHERE tp.providerId = ? AND m.`status`='remain' AND (tp.jobStatus = 'progress' or tp.jobStatus = 'dispute')) AS p, (SELECT IFNULL(SUM(rr.`amount`), 0) AS requested FROM tbl_redeem_requests AS rr WHERE rr.userId = ? AND (rr.`paymentStatus`='pending' OR rr.`paymentStatus`='initiated')) AS r ", array(
                $this->sessUserId,
                $this->sessUserId,
                $this->sessUserId,
            ));
        }

        if ($this->dataOnly) {
            return $result->result();
        } else {
            $result = $result->result();
        }

        $replace = array(
            '%currencySign%' => $_SESSION['sessCurrencySign'],
            '%available%' => number_format(exchange($result['available'], $from = 'USD', $to = $_SESSION['sessCurrencyCode']), 2),
            '%pending%' => number_format(exchange($result['pending'], $from = 'USD', $to = $_SESSION['sessCurrencyCode']), 2),
            '%requested%' => number_format(exchange($result['requested'], $from = 'USD', $to = $_SESSION['sessCurrencyCode']), 2),
            '%totalCredits%' => ($this->sessUserType == 'p') ? $totalCredits : null,
            '%hideCreditSection%' => ($this->sessUserType == 'c') ? 'hide' : null,
            '%hideRequestToRedeem%' => ($this->walletAmount <= 0) ? 'hide' : null,
        );
        return get_view(DIR_TMPL . $this->module . "/balance-nct.tpl.php", $replace);
    }

    public function redeem_rows($count = false)
    {
        $html = null;
        $result = $this->db->select('tbl_redeem_requests', '*', array('userId' => $this->sessUserId), 'ORDER BY id DESC')->results();
        if ($count) {
            return count($result);
        }
        if (!empty($result)) {

            if ($this->dataOnly) {
                return $result;
            }

            foreach ($result as $k => $v) {
                extract($v);
                $replace = array(
                    '%amount%' => $amount,
                    '%currency_sign%' => $_SESSION['sessCurrencySign'],
                    '%amount_calculated%' => number_format(exchange($amount, $from = 'USD', $to = $_SESSION['sessCurrencyCode']), 2),
                    '%createdDate%' => ($createdDate > 0) ? date('d-M-Y', strtotime($createdDate)) : 'N/A',
                    '%description%' => filtering($description),
                    '%paymentStatus%' => ucfirst($paymentStatus),
                    '%redeemedAmount%' => $redeemedAmount,
                    '%redeemedAmount_calculated%' => number_format(exchange($redeemedAmount, $from = 'USD', $to = $_SESSION['sessCurrencyCode']), 2),
                    '%redeemedDate%' => ($redeemedDate > 0) ? date('d-M-Y', strtotime($redeemedDate)) : 'N/A',
                );
                $html .= get_view(DIR_TMPL . $this->module . "/redeem-row-nct.tpl.php", $replace);
            }
        } else {

        }

        return $html;
    }

    public function contactCodeOptions($contactCode)
    {
        global $db;
        $html = null;
        $result = $db->pdoQuery('SELECT DISTINCT phonecode, countryName from tbl_country where isActive="y" order by countryName asc ')->results();

        foreach ($result as $k) {
            $replace = array(
                "%VALUE%" => $k['phonecode'],
                "%SELECTED%" => ($contactCode == $k['phonecode']) ? 'selected' : null,
                "%LABEL%" => $k['countryName'] . " (" . $k['phonecode'] . ")",
                "%TITLE%" => $k['phonecode'],
            );
            $html .= get_view(DIR_TMPL . $this->module . "/" . "option-nct.tpl.php", $replace);
        }
        return $html;
    }

    public function get_redeemModal()
    {
        $walletamount = getTableValue('tbl_users', 'walletamount', array('userId' => $this->sessUserId));
        if ($walletamount > 0) {
            $mailPay = $this->db->select('tbl_users', array('paypalEmail', 'pesapalEmail', 'safaricomContactCode', 'safaricomContactNo'), array('userId' => $this->sessUserId))->result();
            $replace = array(
                '%available%' => $this->walletAmount,
                '%available_calculated%' => $_SESSION['sessCurrencySign'] . number_format(exchange($this->walletAmount, $from = 'USD', $to = $_SESSION['sessCurrencyCode']), 2),
                '%paypalEmail%' => $mailPay['paypalEmail'],
                '%maxAmount%' => $this->walletAmount,
                '%tokenValue%' => setFormToken(),

                '%safaricom_contactCode_options%' => $this->contactCodeOptions($mailPay['safaricomContactCode']),
                '%safaricomContactNo%' => $mailPay['safaricomContactNo'],
            );
            return get_view(DIR_TMPL . $this->module . "/redeemModal-nct.tpl.php", $replace);
        } else {
            $response['status'] = 0;
            $response['msg'] = You_do_not_have_enough_wallet_amount_to_redeem;
            echo json_encode($response);
            exit;
        }

    }

    public function redeem_tab()
    {

        $replace = array(
            '%currency_sign%' => $_SESSION['sessCurrencySign'],
            '%available%' => CURRENCY_SYMBOL . $this->walletAmount,
            '%available_calculated%' => $_SESSION['sessCurrencySign'] . number_format(exchange($this->walletAmount, $from = 'USD', $to = $_SESSION['sessCurrencyCode']), 2),
            '%rows%' => $this->redeem_rows(),
            '%text%' => 'There are no redemption requests yet',
            '%no_data_hide%' => ($this->redeem_rows(true) == 0) ? null : 'hide',
            '%hideRequestToRedeem%' => ($this->walletAmount <= 0) ? 'hide' : null,
        );

        return get_view(DIR_TMPL . $this->module . "/redeem-nct.tpl.php", $replace);
    }

    public function credit_rows($count = false)
    {
        $html = null;
        $result = $this->db->select('tbl_credit_log', '*', array('userId' => $this->sessUserId), 'ORDER BY id DESC')->results();

        if ($this->dataOnly) {
            return $result;
            exit;
        }
        if ($count) {
            return count($result);
        }
        if (!empty($result)) {
            foreach ($result as $k => $v) {
                extract($v);
                $replace = array(
                    '%amount%' => $amount,
                    '%createdDate%' => ($createdDate > 0) ? date('d-M-Y', strtotime($createdDate)) : 'N/A',
                    '%description%' => ucfirst($description),
                );
                $html .= get_view(DIR_TMPL . $this->module . "/credits-row-nct.tpl.php", $replace);
            }
        }
        return $html;
    }

    public function credits_tab()
    {
        $availableCredits = getTableValue('tbl_users', 'totalCredits', array('userId' => $this->sessUserId));
        $replace = array(
            '%available%' => $availableCredits,
            '%rows%' => $this->credit_rows(),
            '%text%' => 'There is no data in your credit history.',
            '%no_data_hide%' => ($this->credit_rows(true) == 0) ? null : 'hide',
        );

        return get_view(DIR_TMPL . $this->module . "/credits-nct.tpl.php", $replace);
    }

    public function getPageContent()
    {
        $replace = array('%tab_panel%' => $this->balance_tab(), '%hideCreditSection%' => ($this->sessUserType == 'c') ? 'hide' : null, '%currencySign%' => $_SESSION['sessCurrencySign'], '%currencyCode%' => $_SESSION['sessCurrencyCode']);
        return get_view(DIR_TMPL . $this->module . "/" . $this->module . ".tpl.php", $replace);
    }

}
