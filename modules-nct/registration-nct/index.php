<?php
$reqAuth = false;
$module  = 'registration-nct';
require_once "../../includes-nct/config-nct.php";
require_once "class.registration-nct.php";

extract($_REQUEST);

$winTitle =$headTitle= Signup.' - ' . SITE_NM;

$metaTag = getMetaTags(array(
    "description" => $winTitle,
    "keywords"    => $headTitle,
    "author"      => AUTHOR,
));

if ($sessUserId > 0) {
    $msgType = $_SESSION['msgType'] = disMessage(array(
        "type" => "suc",
        "var"  => You_are_already_logged_in
    ));
    redirectPage(SITE_URL);
} else if (isset($_POST['submitSignup'])) {
    //dump_exit($_POST);

    extract($_POST);
    ///////////////////
    if(!isset($dataOnly) || !$dataOnly){
    if (!checkFormToken($token)) {
        $_SESSION["msgType"] = disMessage(array(
            'type' => 'err',
            'var'  => "Invalid request.",
        ));
        redirectPage(SITE_REGISTER);

    }
}
///////////////////
    $email    = isset($email) ? $email : '';
    $password = isset($password) ? $password : '';
    $cpass    = isset($password_confirmation) ? $password_confirmation : '';

    if ($email != '' && $password != '' && $firstName != null && $lastName != null && $userType != null && $userName != null) {
        $isExist = $db->select('tbl_users', array('userId'), array('email' => $email), ' LIMIT 1');        

        if ($isExist->affectedRows() > 0) {
            $_SESSION["msgType"] = disMessage(array(
                'type' => 'err',
                'var'  => Email_you_have_entered_is_already_registered,
            ));
            redirectPage(SITE_REGISTER);
        } else {
            $insertarray = array(
                "email"       => $email,
                "password"    => md5($password),
                "userName"    => $userName,
                "profileLink" => $userName,
                "countryCode" => $countryCode,
                "contactCode" => $contactCode,
                "contactNo"   => $contactNo,
                "firstName"   => $firstName,
                "lastName"    => $lastName,
                "userType"    => $userType,
                "isActive"    => 'n',
                "ipAddress"   => get_ip_address(),
                "createdDate" => date('Y-m-d H:i:s'),
            );

            $activationCode = $insertarray['activationCode'] = md5(time());
            $activationLink = SITE_URL . 'active-account/' . $activationCode;

            $insert_id = $db->insert('tbl_users', $insertarray)->getLastInsertId();

            $to        = $email;
            $arrayCont = array(
                'greetings'      => $firstName,
                'activationLink' => $activationLink,
            );
            $_SESSION['sendMailTo'] = issetor($insert_id,0); 

            $array = generateEmailTemplate('user_register', $arrayCont);
            //echo $array['message'];exit;
            sendEmailAddress($to, $array['subject'], $array['message']);

            /*welcome newsletter mail*/
	    /*	
            $qrySelNL                        = $db->select("tbl_newsletters", "*", array("id" => 1))->result();
            $arrayCont                       = array();
            $arrayCont['subject']            = $qrySelNL['newsletter_subject'];
            $arrayCont['newsletter_content'] = $qrySelNL['newsletter_content'];
            $arrayCont['greetings']          = $firstName;

            $array = generateEmailTemplate('newsletter', $arrayCont);
            sendEmailAddress($to, $arrayCont['subject'], $array['message']);
            */
            $msgType = $_SESSION["msgType"] = disMessage(array(
                'type' => 'suc',
                'var'  => successfully_registered_part1.' ' . SITE_NM.'. '.successfully_registered_part2
            ));
            redirectPage(SITE_LOGIN);
        }
    } else {
        $msgType = $_SESSION["msgType"] = disMessage(array(
            'type' => 'err',
            'var'  => You_must_enter_all_the_required_details_carefully
        ));
        redirectPage(SITE_REGISTER);
    }
}

$obj = new Registration($module, $_REQUEST);

$pageContent = $obj->getPageContent();

require_once DIR_TMPL . "parsing-nct.tpl.php";
