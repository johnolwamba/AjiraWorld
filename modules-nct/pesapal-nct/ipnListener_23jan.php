<?
require_once '../../includes-nct/config-nct.php';
include_once DIR_INC . 'pesapal-nct/OAuth.php';
global $db;
$dbUpdate = '';
$reqAuth = false;

foreach ($_REQUEST as $k => $v) {
  $content .= "\n\nkey=" . $k . "======>value=" . $v."\n\n";
}
$h = fopen("notify_IPN.txt", "a");
$r = fwrite($h, $content);
fclose($h);

// Parameters sent to you by PesaPal IPN
$pesapalNotification= isset($_GET['pesapal_notification_type']) ? $_GET['pesapal_notification_type'] : '';
$pesapalTrackingId=isset($_GET['pesapal_transaction_tracking_id']) ? $_GET['pesapal_transaction_tracking_id'] : '';
$pesapal_merchant_reference=isset($_GET['pesapal_merchant_reference']) ? base64_decode($_GET['pesapal_merchant_reference']) : '';

$log_array = print_r($_REQUEST, true);

//$consumer_key = PESAPAL_CONSUMER_KEY; //Register a merchant account on
$consumer_key = PESAPAL_CONSUMER_KEY;
$consumer_secret = PESAPAL_SECRET_KEY;
$statusrequestAPI = PESAPAL_IPN;//change to

$string = $pesapal_merchant_reference;
$refData = explode(',', $string);
$typePay = $refData[0]; //payment type is deposit to wallet or membership
$amount = $refData[3];

if($pesapalNotification =="CHANGE" && $pesapalTrackingId!=''){
  $token = $params = NULL;
  $consumer = new OAuthConsumer($consumer_key, $consumer_secret);
  $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

   //get transaction status
  $request_status = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $statusrequestAPI, $params);
  $request_status->set_parameter("pesapal_merchant_reference", base64_encode($pesapal_merchant_reference));
  $request_status->set_parameter("pesapal_transaction_tracking_id",$pesapalTrackingId);
  $request_status->sign_request($signature_method, $consumer, $token);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $request_status);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HEADER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

  if(defined('CURL_PROXY_REQUIRED')) if (CURL_PROXY_REQUIRED == 'True'){
    $proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
    curl_setopt ($ch, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
    curl_setopt ($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
    curl_setopt ($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
  }
  $response = curl_exec($ch);
  $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
  $raw_header  = substr($response, 0, $header_size - 4);
  $headerArray = explode("\r\n\r\n", $raw_header);
  $header      = $headerArray[count($headerArray) - 1];

  $elements = preg_split("/=/",substr($response, $header_size));
  $status = $elements[1];

  curl_close ($ch);

  $content = '';
  $content = "\n\n-----------------------------------------------------------------------------------" . date("Y-m-d H:i:s");
  foreach ($_REQUEST as $k => $v) {
    $content .= "\n\nkey=" . $k . "======>value=" . $v."\n\n";
  }
  foreach ($refData as $k => $v) {
    $content .= "\n\nkey=" . $k . "======>value=" . $v."\n\n";
  }
  $content .= "\n";
  $content .=  print_r($response, true);
  $h = fopen("notify.txt", "a");
  $r = fwrite($h, $content);
  fclose($h);

  if($typePay == 'dw'){
    $userId = $refData[1];
    $payhistoryId = $refData[2];

    $user_details = $db->select('tbl_users', array('firstName','email','pesapalEmail'), array('userId' => $userId))->result();
    if($status == 'COMPLETED'){
      $txn_exist = getTableValue('tbl_payment_history', 'id',array('transactionId'=>$pesapalTrackingId,'paymentStatus'=>'completed'));
      if(!$txn_exist){
        $admincommision = $amount * DEPOSIT_COMMISSION / 100;
        $wallet = $amount - $admincommision;
        $dbUpdate = $db->update('tbl_payment_history', 
            array('paymentStatus'=> 'completed',
                  'adminCommission' => $admincommision,
                  'transactionId' => $pesapalTrackingId), array(
          'id'     => $payhistoryId,
          'userId' => $userId))->affectedRows();
        $db->pdoQuery('UPDATE tbl_users SET walletAmount = walletAmount + ? WHERE userId=?', array((string) $wallet,$userId));  
      }
    }
    if($status == 'FAILED'){
      $dbUpdate = $db->update('tbl_payment_history', array('paymentStatus'=> 'failed'), array('id'=> $payhistoryId,'userId' => $userId))->affectedRows();
    }  
  }
  if($typePay == 'mem'){
    $objPost = new stdClass();  
    $userId = $refData[1];
    $membershipId = $refData[2];

    $tobeAdded = getTableValue('tbl_memberships', 'credits',array('id'=>$membershipId));
    $planName = getTableValue('tbl_memberships', 'membership',array('id'=>$membershipId));

    if($status == 'COMPLETED'){
      $objPost->userId =  $userId;
      $objPost->transactionId =  $_GET['pesapal_transaction_tracking_id'];
      $objPost->paymentType   =  "buy membership";   
      $objPost->membershipId  =  $membershipId;
      //$objPost->pesapal_fees   =  $pesapalCommision;
      $objPost->creditBought   =  $tobeAdded;
      $objPost->paymentStatus = 'completed';
      $objPost->ipAddress   	 =  get_ip_address();
      $objPost->createdDate 	 =  date('Y-m-d H:i:s');
      $objPost->is_cancled_user =  'n';
      $objPost->gateway  = 'pe';

      $txn_exist = getTableValue('tbl_payment_history', 'id',array('transactionId'=>$pesapalTrackingId,'paymentStatus'=>'completed'));
      if(!$txn_exist){
         $last_id = $db->insert('tbl_payment_history',(array)$objPost)->lastInsertId();
         $dbUpdate = $db->pdoQuery('UPDATE tbl_users SET totalCredits=totalCredits+? WHERE userId=?', array($tobeAdded, $userId))->affectedRows(); 
        
         $sql = $db->select('tbl_users','*',array('userId'=>$userId))->result();
         $db->insert('tbl_credit_log', array(
          'userId' => $userId, 
          'amount' => (string)$tobeAdded, 
          'transactionType' => 'buy membership', 
          'createdDate' => date('Y-m-d H:i:s'), 
          'description' => $tobeAdded.' credits added in reference to your purchase of '.$planName.' plan.', 
         ));
         if(isset($sql['userId']) && $sql['userId']> 0){                       
          $activationLink = SITE_URL;
          $mailarray = array('greetings'=>$sql['firstName'],'activationLink'=>$activationLink);
          $array = generateEmailTemplate('purchase_membership_plan',$mailarray);
          sendEmailAddress($sql['email'],$array['subject'],$array['message']);        
         }
      }
    }
    if($status == 'FAILED'){
      $objPost->userId =  $userId;
      $objPost->transactionId =  $_GET['pesapal_transaction_tracking_id'];
      $objPost->paymentType   =  "buy membership";   
      $objPost->membershipId  =  $membershipId;
      $objPost->pesapal_fees   =  $pesapalCommision;
      $objPost->creditBought   =  $tobeAdded;
      $objPost->jsonDetails   =  json_encode($log_array);
      $objPost->paymentStatus = 'failed';
      $objPost->ipAddress   	 =  get_ip_address();
      $objPost->createdDate 	 =  date('Y-m-d H:i:s');
      $objPost->is_cancled_user =  'n';
      $objPost->gateway  = 'pe';

      $last_id = $db->insert('tbl_payment_history',(array)$objPost)->lastInsertId();
    }
  }
  if($dbUpdate > 0){
    ob_start();
    echo "<pre>";
    echo $log_array;
    echo "</pre>";
    ob_flush();
  }else{
    ob_start();
    echo "<pre>";
    echo $log_array;
    echo "Transaction done already";
    echo "</pre>";
    ob_flush();
  }
}