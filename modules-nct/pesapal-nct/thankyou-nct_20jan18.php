<?php
$reqAuth = false;
$content = '';
$content = "\n\n-----------------------------------------------------------------------------------" . date("Y-m-d H:i:s");
foreach ($_REQUEST as $k => $v) {
	$content .= "\n\nkey=" . $k . "======>value=" . $v;
}
$h = fopen("notify.txt", "a");
$r = fwrite($h, $content);
fclose($h);

require_once '../../includes-nct/config-nct.php';
global $db;

$type = (!empty($_REQUEST['f']) ? base64_decode(trim($_REQUEST['f'])) : '');
$type = trim($type);
$postData = '';
foreach ($_POST as $id => $val) {
	$postData .= $id . '-' . $val . ' | ';
}
sendEmailAddress('krunal.limbad@ncrypted.com', 'Thankyou - ' . trim($type) . ' - ' . SITE_NM, $postData);

if ($type == 'deposit') {
	extract($_REQUEST);
	$string = base64_decode($_GET['pesapal_merchant_reference']);
	$refData = explode(',', $string); //first will be userId, second will be payment history id

	if (!empty($userId) && !empty($payhistoryId) && !empty($amount)) {
		$userId = $refData[1];
		$payhistoryId = $refData[2];
		$amount = $refData[3];
		
		$user_details = $db->select('tbl_users', array(
			'firstName',
			'email',
			'pesapalEmail'), array('userId' => $userId))->result();

		//send acknowledge mail
		$arrayCont = array(
			'greetings' => $user_details['firstName'],
			'USER_EAMIL' => $user_details['email'],
			'PAYPAL_ACCOUNT' => $user_details['pesapalEmail'],
			'TRANSACTION_ID' => $_GET['pesapal_transaction_tracking_id'],
			'AMOUNT' => CURRENCY_SYMBOL . $amount,
			'DATE' => date('d-m-Y'),
			'STATUS' => 'In progress',
		);
		$db->update('tbl_payment_history', array('transactionId' => $_GET['pesapal_transaction_tracking_id']), array('id' => $refData[1]));

		$array = generateEmailTemplate('deposit_to_wallet', $arrayCont);
		//echo $array['message'];exit;
		sendEmailAddress($user_details['email'], $array['subject'], $array['message']);
	}
	$_SESSION["msgType"] = disMessage(array(
		'type' => 'suc',
		'var' => 'Payment status for this transaction is pending. Amount will be added to your wallet once completed.',
	));

	redirectPage(SITE_WALLET);
}
if($type == 'membership'){
	$_SESSION['random_number'] = md5(rand(1, 50));
	
	if($sessUserId == 0){
		redirectPage(SITE_URL);
	}
	else{	
		if(isset($_SESSION['random_number']) && $_SESSION['random_number']!=''){?>
		    <br /><br /><center><h1> <font color="#009900">Your membership plan has been successfully purchased.Please wait.....</font></h1><center>     
		  <?php   unset($_SESSION['random_number']);
			$_SESSION["msgType"] = disMessage(array(
                    'type' => 'suc',
                    'var' => "Your membership plan has been successfully purchased. Please wait some minute(s) for updates..."
        	));
        	redirectPage(SITE_MEM_PLANS);  
       	}
		else{
			redirectPage(SITE_URL); 
		}
	}   
}
?>