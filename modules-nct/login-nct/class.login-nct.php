<?php
class Login {
    function __construct($module = "", $id = 0, $token = "", $reffToken = "") {
        foreach ($GLOBALS as $key => $values) {
            $this -> $key = $values;
        }
        $this -> module = $module;
        $this -> id = $id;

    }

    public function getPageContent() {
        // print_r($_COOKIE);exit;
        // echo $_COOKIE["Ajira_WorlduserName"];exit;
        $replace = array(
            '%userName%' => (isset($_COOKIE["Ajira_WorlduserName"]) && $_COOKIE["Ajira_WorlduserName"] != '') ? $_COOKIE["Ajira_WorlduserName"] : NULL,
            '%password%' => (isset($_COOKIE["Ajira_Worldpassword"]) && $_COOKIE["Ajira_Worldpassword"] != '') ? $_COOKIE["Ajira_Worldpassword"] : NULL,
            '%remember_me%' => (isset($_COOKIE["Ajira_Worldrememberme"]) && $_COOKIE["Ajira_Worldrememberme"] == 'y') ? 'checked="checked"' : NULL,
            '%tokenValue%' => setFormToken()
        );
        return get_view(DIR_TMPL . $this -> module . "/" . $this -> module . ".tpl.php", $replace);
    }

    public function getForgetPage() {
        return get_view(DIR_TMPL . $this -> module . "/forget-nct.tpl.php",array(
            '%tokenValue%' => setFormToken()));
    }

}
?>
