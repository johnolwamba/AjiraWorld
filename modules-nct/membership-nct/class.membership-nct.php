<?php
class Membership {
	function __construct($module = "", $id = 0, $token = "", $reqData = array(), $reffToken = "") {
		global $js_variables;
		foreach ($GLOBALS as $key => $values) {
			$this->$key = $values;
		}
		$this->module = $module;
		$this->id = $id;
		$this->reqData = $reqData;

		//for web service
		$this->dataOnly = (isset($this->reqData['dataOnly']) && $this->reqData['dataOnly'] == true) ? true : false;
		$this->sessUserId = (isset($reqData['userId']) && $reqData['userId'] > 0) ? $reqData['userId'] : $this->sessUserId;

		$this->userData = getUserData();
		$this->userMembership = getUserMembership();
		$js_variables = "var credit_bunch ='" . $this->getAdminCreditPlan(true) . "'";
	}

	public function getAdminCreditPlan($onlyCredits = false, $onlyPrice = false) {
		$plan = $this->db->select('tbl_credit_plans', '*', array('isActive' => 'y'))->result();
		if (!empty($plan)) {
			if ($onlyCredits) {
				return $plan['credits'];
			}

			if ($onlyPrice) {
				return $plan['price'];
			}

			return $plan;
		} else {
			return false;
		}
	}
	public function chooseGatewayModal() {
		$mailPay = $this->db->select('tbl_users', array('paypalEmail', 'pesapalEmail'), array('userId' => $this->sessUserId))->result();
		$paycheck = $mailPay['paypalEmail'] != '' ? 'checked' : '';
		$pescheck = $mailPay['paypalEmail'] == '' ? 'checked' : '';

		$replace = array(
			'%PAYCHECKED%' => $paycheck,
			'%PESACHECKED%' => $pescheck);

		return get_view(DIR_TMPL . $this->module . "/paymodal-nct.tpl.php", $replace);

	}
	public function getPageContent() {
		$results = $this->db->select('tbl_memberships', '*', array('isActive' => 'y'));

		if ($this->dataOnly) {
			return $results->results();
			exit;
		} else {
			$results = $results->results();
		}

		$html = null;
		foreach ($results as $k => $v) {
			$replace = array(
				'%membership%' => $v['membership'],
				'%description%' => $v['description'],
				'%credits%' => $v['credits'],
				'%price%' => CURRENCY_SYMBOL . $v['price'],
				'%id%' => $v['id'],
				'%planid%' => ($v['id'] == $this->userMembership['membershipId']) ? 'javascript:void(0);' : $v['id'],
				'%buy_button%' => ($v['id'] == $this->userMembership['membershipId']) ? 'Your Current Plan' : 'Buy',

			);
			$html .= get_view(DIR_TMPL . $this->module . "/membership-row-nct.tpl.php", $replace);
		}

		$plan = $this->db->select('tbl_credit_plans', '*', array('isActive' => 'y'))->result();

		return get_view(DIR_TMPL . $this->module . "/" . $this->module . ".tpl.php", array(
			'%plans%' => $html,
			'%credit_plans%' => (!empty($plan))?$this->creditPlans():null,
			'%BUNCH_PRICE%' => $this->getAdminCreditPlan(false, true),
			'%CREDIT_BUNCH%' => $this->getAdminCreditPlan(true),
			'%tokenValue%' => setFormToken()));
	}

	public function creditPlans() {
		return get_view(DIR_TMPL . $this->module . "/" . "credit_plans-nct.tpl.php");
	}

}
?>
