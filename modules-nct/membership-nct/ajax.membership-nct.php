<?php
$reqAuth = false;
$module = 'membership-nct';
require_once "../../includes-nct/config-nct.php";
require_once "class.membership-nct.php";

$obj = new Membership();

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : NULL;
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : NULL;
$gateway = isset($_REQUEST['gateway']) ? $_REQUEST['gateway'] : NULL;
$table = 'tbl_memberships';
if ($action == 'buy' && $id > 0 && $gateway == 'pa') {
	extract($_REQUEST);

	$date = date('Y-m-d H:i:s');
	$ip = get_ip_address();

	$planData = $db->select($table, '*', array('id' => $id))->result();

	if ($planData['price'] > 0) {
		$replace = array(
			'%membershipId%' => $id,
			'%userId%' => $sessUserId,
			'%finalPrice%' => $planData['price'],
		);
		echo get_view(DIR_TMPL . $module . "/membership-form-nct.tpl.php", $replace);
	} else {
		$_SESSION["msgType"] = disMessage(array(
			'type' => 'err',
			'var' => 'Price for this plan is not adequate to make this payment. Kindly try another plan.',
		));
		redirectPage(SITE_MEM_PLANS);
	}

} elseif ($action == 'buy' && $id > 0 && $gateway == 'pe') {
	extract($_REQUEST);

	$date = date('Y-m-d H:i:s');
	$ip = get_ip_address();

	$planData = $db->select($table, '*', array('id' => $id))->result();

	if ($planData['price'] > 0) {
		$type = base64_encode('membership');
		$exString = 'mem,'.$sessUserId.','.$id.','.$planData['price'];
		$frameString = pesaPay($sessUserId, $planData['price'], get_link('pespal_thankyou',$type),base64_encode($exString));
		redirectPage($frameString);
	} else {
		$_SESSION["msgType"] = disMessage(array(
			'type' => 'err',
			'var' => 'Price for this plan is not adequate to make this payment. Kindly try another plan.',
		));
		redirectPage(SITE_MEM_PLANS);
	}
} elseif ($action == "adHocCredits") {

	extract($_POST);
	///////////////////
	if (!isset($dataOnly) || !$dataOnly) {
		if (!checkFormToken($token)) {
			$response['status'] = 0;
			$response['msg'] = "Invalid request.";
			$response['newToken'] = setFormToken();
			echo json_encode($response);
			exit;
		}
	}
	///////////////////

	if ($credits > 0) {

		//get credits required by admin
		$credits_bunch = $obj->getAdminCreditPlan(true);
		if ($credits % $credits_bunch !== 0) {
			$response['status'] = 0;
			$response['msg'] = 'You can avail credits only in the multiples of ' . $credits_bunch;
			echo json_encode($response);
			exit;
		}

		//check if sufficient balance?
		$requiredBalance = ($credits / $obj->getAdminCreditPlan(true)) * $obj->getAdminCreditPlan(false, true);
		$check = $db->select('tbl_users', 'userId', array(
			'walletamount >=' => (string) $requiredBalance,
			'userId' => $sessUserId,
			'userType' => 'p',
		))->result();

		//if yes then prpceed
		if (isset($check) && $check > 0) {
			//update walletamount
			$updwalletresults = $db->pdoQuery("UPDATE tbl_users set walletamount = walletamount-$requiredBalance where userId=? AND userType='p'", array($sessUserId));

			//update totalCredits
			$updcreditresults = $db->pdoQuery("UPDATE tbl_users set totalCredits = totalCredits+$credits where userId=? AND userType='p'", array($sessUserId));

			//add log
			$db->insert('tbl_credit_log', array(
				'userId' => $sessUserId,
				'amount' => $credits,
				'transactionType' => 'adhoc purchase',
				'createdDate' => date('Y-m-d H:i:s'),
				'description' => 'Credits added in reference to your adhoc credit purchase of ' . $credits . ' credits against sum of ' . $requiredBalance . ' wallet amount.',
			));

			$userDetails = $db->select('tbl_users', '*', array('userId' => $sessUserId))->result();

			$array = generateEmailTemplate('credit_added', array(
				'greetings' => ucfirst($userDetails['firstName']),
				'tot_credits' => $credits,
				'tot_paid' => $requiredBalance,
				'date' => date('d M Y'),

			));
			//echo '<br/>'.$array['message'];exit;
			sendEmailAddress($userDetails['email'], $array['subject'], $array['message']);

			if ($updwalletresults->affectedRows() > 0 && $updcreditresults->affectedRows() > 0) {
				$response['status'] = 1;
				$response['msg'] = "Adhoc credit has been credicted successfully and amount has been deducted from your wallet.";
			} else {
				$response['status'] = 0;
				$response['msg'] = "Something went wrong while updating your credit balance.";
			}
		} else {
			$response['status'] = 0;
			$response['msg'] = "You do not have sufficient wallet balance to purchase credits.";
		}

	} else {
		$response['status'] = 0;
		$response['msg'] = "Incorrect number of credits. Please check again.";
	}
	echo json_encode($response);
	exit;
} elseif ($action == "chooseGateway"){
	
}
?>