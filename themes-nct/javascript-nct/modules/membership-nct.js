var clickId;

// Add custom validation rule
$.formUtils.addValidator({
    name: 'multiple_of_number',
    validatorFunction: function(value, $el, config, language, $form) {
        if (value == "") {
            $el.attr("data-validation-error-msg", lang.Please_enter_required_credits);
            $('.credit-box span').html(0.00);
            return false;
        }else if (value < 0) {
            $el.attr("data-validation-error-msg", lang.negative_values_are_not_allowed);
            $('.credit-box span').html(0.00);
            return false;
        } else if (parseInt(value) == 0) {
            $el.attr("data-validation-error-msg", lang.Entered_number_of_credits_is_invalid_Please_check);
            $('.credit-box span').html(0.00);
            return false;
        } else if (parseInt(value) % credit_bunch === 0) {
            return true;
        } else {
            $el.attr("data-validation-error-msg", lang.You_can_avail_credits_only_in_the_multiples_of_X + ' ' + credit_bunch);
            $('.credit-box span').html(0.00);
            return false;
        }

    },
    errorMessage: lang.You_can_avail_credits_only_in_the_multiples_of_X + ' ' + credit_bunch,
    errorMessageKey: 'multiple_of_number'
});


$(document).on('click', '[data-ele="gatewayModal"]', function(e) {
    clickId = this.id;
    var checked = $('input[name="choosepay"]:checked').val();
    submitValueHandler(window.location.href, 'action=method&method=chooseGatewayModal&gateway=' + checked + '&id=' + clickId, 'Please wait..', function(data) {
        $('body').append(data.html);
        $('#m_plan').val(clickId);
        $("#gateWayModal").modal('show');
    });
});

$(document).on('submit', '#chooseGateway', function() {

});

$(document).on('shown.bs.modal', '#gateWayModal', function(e) {
    $.validate();
});

$(document).on('click', '[data-ele="submitAdhocCrediForm"]', function(e) {
    e.preventDefault();
    submitFormHandler(ajaxUrl, 'adhocCrediForm', 'Processing..', function(data) {
        $('[data-ele="credits"]').val("0");
    });
});