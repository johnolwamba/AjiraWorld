<?php 
	error_reporting(0);
	

	extract($_POST);
	
	require_once ("../../../includes-nct/config-nct.php");
	require_once ("Mpesa.php");
	
	$_SESSION['SAFARI_CONSUMER_KEY'] = SAFARI_CONSUMER_KEY;
	$_SESSION['SAFARI_CONSUMER_SECRET'] = SAFARI_CONSUMER_SECRET;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		  <title>Safaricom B2C Test</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h2>Safaricom B2C Test</h2>
			<form class="form-horizontal" method="post" name="b2cform" autocomplete="off">
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="ConsumerKey">Consumer Key:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= $_SESSION['SAFARI_CONSUMER_KEY'] ?>" required id="ConsumerKey" placeholder="Enter ConsumerKey" name="ConsumerKey">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="ConsumerSecret">Consumer Secret:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= $_SESSION['SAFARI_CONSUMER_SECRET'] ?>" required id="ConsumerSecret" placeholder="Enter ConsumerSecret" name="ConsumerSecret">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="InitiatorName">InitiatorName:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($InitiatorName)?$InitiatorName:'' ?>" required id="InitiatorName" placeholder="Enter InitiatorName" name="InitiatorName">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="SecurityCredential">SecurityCredential:</label>
			      	<div class="col-sm-10">
			      		<textarea class="form-control" required id="SecurityCredential" placeholder="Enter SecurityCredential" rows="5" name="SecurityCredential"><?= isset($SecurityCredential)?$SecurityCredential:'' ?></textarea>
			        	<!-- <input type="text" class="form-control" value="<?= isset($SecurityCredential)?$SecurityCredential:'' ?>" required id="SecurityCredential" placeholder="Enter SecurityCredential" name="SecurityCredential"> -->
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="CommandID">CommandID:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($CommandID)?$CommandID:'' ?>" required id="CommandID" placeholder="Enter CommandID" name="CommandID">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="Amount">Amount:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($Amount)?$Amount:'' ?>" required id="Amount" placeholder="Enter Amount" name="Amount">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="PartyA">PartyA:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($PartyA)?$PartyA:'' ?>" required id="PartyA" placeholder="Enter PartyA" name="PartyA">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="PartyB">PartyB:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($PartyB)?$PartyB:'' ?>" required id="PartyB" placeholder="Enter PartyB" name="PartyB">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="Remarks">Remarks:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($Remarks)?$Remarks:'' ?>" required id="Remarks" placeholder="Enter Remarks" name="Remarks">
			      	</div>
			    </div>			    
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="Occasion">Occasion:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($Occasion)?$Occasion:'' ?>" required id="Occasion" placeholder="Enter Occasion" name="Occasion">
			      	</div>
			    </div>

			    <div class="form-group">        
			      	<div class="col-sm-offset-2 col-sm-10">
			        	<button type="submit" value="submitB2C" name="submit" class="btn btn-default">Submit</button>
			        	<!-- <button type="reset" value="Reset" class="btn btn-default">Reset</button> -->
			      	</div>
			    </div>
			</form>

			<div class="form-group">
		      	<label class="control-label" for="QueueTimeOutURL">QueueTimeOutURL :</label> http://ajira.world.ncryptedprojects.com/admin-nct/modules-nct/redeem_request-nct/result.php
		    </div>

		    <div class="form-group">
		      	<label class="control-label" for="ResultURL">ResultURL :</label> http://ajira.world.ncryptedprojects.com/admin-nct/modules-nct/redeem_request-nct/result.php
		    </div>

		    <div class="form-group">
		      	<label class="control-label" for="ResultURL">ResultURL Response :</label> http://ajira.world.ncryptedprojects.com/upload-nct/notify.txt
		    </div>

		</div>
	</body>
</html>
<?php	
	//extract($_POST);
	if(isset($submit) && $submit == 'submitB2C'){
		// require_once ("../../../includes-nct/config-nct.php");
		// require_once ("Mpesa.php");
		$_SESSION['SAFARI_CONSUMER_KEY'] = $ConsumerKey;
		$_SESSION['SAFARI_CONSUMER_SECRET'] = $ConsumerSecret;

		$mpesa= new \Safaricom\Mpesa\Mpesa();

		// $InitiatorName = 'safaricom.7';
		// $SecurityCredential = 'UZZQCQKLY8z0GM/nPiaxoCYRKTGEdjOU03eHVoFbs0EUdAB2PI+1otTVuZ37hSlTCAMehorrPNY9g5yTIHhI3e98kFo6EkFeTFXEOtmWPdLlOjbFz3CvJDpqpP2KbBUICRMxK0h6TZaiqoFyWMRL0XIJiDFLXakHTD3ntfyt0Hg0GvBMh1cwGe2Y/YelKsfzoW+Q5tC5pEVhCFmky230EIWsLBqFsOMbAo4fdYygCnT+8ufXzxcqrkRD78gkbQTvPyUWJAqjZbrFazBRJWaeXjRZtpNRrzu0Twhc7LsXfV/t4ae1LZnattym2cX0X0+YQGTL6LqVgJBhbn4ULHdFmw=='; 
		// $CommandID = 'BusinessPayment';
		// $Amount = '10';
		// $PartyA = '600000';
		// $PartyB = '254708374149';
		// $Remarks = 'test the safaricom';
		// $Occasion = 'Test Payment for Safaricom';

		$QueueTimeOutURL = 'http://ajira.world.ncryptedprojects.com/admin-nct/modules-nct/redeem_request-nct/result.php';
		$ResultURL = 'http://ajira.world.ncryptedprojects.com/admin-nct/modules-nct/redeem_request-nct/result.php';


		$b2cTransaction=$mpesa->b2c($InitiatorName, $SecurityCredential, $CommandID, $Amount, $PartyA, $PartyB, $Remarks, $QueueTimeOutURL, $ResultURL, $Occasion);

		echo "<div class='container'><h2>Safaricom B2C Response</h2><pre>".$b2cTransaction."</pre></div>";
		// $b2cTransaction = json_decode($b2cTransaction);



		// if(isset($b2cTransaction->ResponseCode) && $b2cTransaction->ResponseCode == '0'){
		// 	echo "ConversationID : ".$b2cTransaction->ConversationID;
		// 	echo "OriginatorConversationID : ".$b2cTransaction->OriginatorConversationID;
		// 	echo "ResponseDescription : ".$b2cTransaction->ResponseDescription;
		// }
		// else{
		// 	echo "errorCode : ".$b2cTransaction->errorCode;
		// 	echo "requestId : ".$b2cTransaction->requestId;
		// 	echo "errorMessage : ".$b2cTransaction->errorMessage;
		// }
	}
?>