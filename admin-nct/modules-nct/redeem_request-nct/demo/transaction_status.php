<?php
	header("Cache-Control: no cache");
	session_cache_limiter("private_no_expire"); 
	error_reporting(0);
	extract($_POST);	
	
	require_once ("../../../../includes-nct/config-nct.php");
	
	$_SESSION['SAFARI_CONSUMER_KEY'] = SAFARI_CONSUMER_KEY;
	$_SESSION['SAFARI_CONSUMER_SECRET'] = SAFARI_CONSUMER_SECRET;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		  <title>Safaricom Transaction Status</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h2>Safaricom Transaction Status</h2>
			<form class="form-horizontal" method="post" name="TransactionStatusform" autocomplete="off">
			    
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="accessToken">accessToken:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($accessToken)?$accessToken:'' ?>" required id="accessToken" placeholder="Enter accessToken" name="accessToken">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="Initiator">Initiator:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($Initiator)?$Initiator:'' ?>" required id="Initiator" placeholder="Enter Initiator" name="Initiator">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="CommandID">CommandID:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($CommandID)?$CommandID:'' ?>" required id="CommandID" placeholder="Enter CommandID" name="CommandID">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="TransactionID">TransactionID:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($TransactionID)?$TransactionID:'' ?>" required id="TransactionID" placeholder="Enter TransactionID" name="TransactionID">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="SecurityCredential">SecurityCredential:</label>
			      	<div class="col-sm-10">
			      		<textarea class="form-control" required id="SecurityCredential" placeholder="Enter SecurityCredential" rows="5" name="SecurityCredential"><?= isset($SecurityCredential)?$SecurityCredential:'' ?></textarea>			        	
			      	</div>
			    </div>			    
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="PartyA">PartyA:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($PartyA)?$PartyA:'' ?>" required id="PartyA" placeholder="Enter PartyA" name="PartyA">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="IdentifierType">IdentifierType:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($IdentifierType)?$IdentifierType:'' ?>" required id="IdentifierType" placeholder="Enter IdentifierType" name="IdentifierType">
			      	</div>
			    </div>

			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="Remarks">Remarks:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($Remarks)?$Remarks:'' ?>" required id="Remarks" placeholder="Enter Remarks" name="Remarks">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="Occasion">Occasion:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($Occasion)?$Occasion:'' ?>" required id="Occasion" placeholder="Enter Occasion" name="Occasion">
			      	</div>
			    </div>
			    <div class="form-group">        
			      	<div class="col-sm-offset-2 col-sm-10">
			        	<button type="submit" value="submitTransactionStatus" name="submit" class="btn btn-default">Submit</button>
			      	</div>
			    </div>
			</form>

			<div class="form-group">
		      	<label class="control-label" for="QueueTimeOutURL">QueueTimeOutURL :</label> http://ajira.world.ncryptedprojects.com/admin-nct/modules-nct/redeem_request-nct/result.php
		    </div>

		    <div class="form-group">
		      	<label class="control-label" for="ResultURL">ResultURL :</label> http://ajira.world.ncryptedprojects.com/admin-nct/modules-nct/redeem_request-nct/result.php
		    </div>

		    <div class="form-group">
		      	<label class="control-label" for="ResultURL">ResultURL Response :</label> http://ajira.world.ncryptedprojects.com/upload-nct/notify.txt
		    </div>

		</div>
	</body>
</html>
<?php		
	if(isset($submit) && $submit == 'submitTransactionStatus'){

		$url = 'https://sandbox.safaricom.co.ke/mpesa/transactionstatus/v1/query';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$accessToken.'')); //setting custom header


		$curl_post_data = array(
		  //Fill in the request parameters with valid values
		  'Initiator' => $Initiator,
		  'SecurityCredential' => $SecurityCredential,
		  'CommandID' => $CommandID,
		  'TransactionID' => $TransactionID,
		  'PartyA' => $PartyA,
		  'IdentifierType' => $IdentifierType,
		  'QueueTimeOutURL' => 'http://ajira.world.ncryptedprojects.com/admin-nct/modules-nct/redeem_request-nct/result.php',
		  'ResultURL' => 'http://ajira.world.ncryptedprojects.com/admin-nct/modules-nct/redeem_request-nct/result.php',
		  'Remarks' => $Remarks,
		  'Occasion' => $Occasion
		);

		$data_string = json_encode($curl_post_data);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

		$curl_response = curl_exec($curl);


		echo "<div class='container'><h2>Safaricom Transaction Status Response</h2><pre>".$curl_response."</pre></div>";
		
	}
?>