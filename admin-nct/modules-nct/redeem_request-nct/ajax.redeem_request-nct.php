<?php
	$content = '';
	require_once("../../../includes-nct/config-nct.php");
	require_once ("Mpesa.php");

	if($adminUserId == 0){die('Invalid request');}
	include("class.redeem_request-nct.php");

	$module = 'redeem_request-nct';
	chkPermission($module);
	$Permission=chkModulePermission($module);
	$table = 'tbl_redeem_requests';
	$action = isset($_GET["action"]) ? trim($_GET["action"]) : (isset($_POST["action"]) ? trim($_POST["action"]) : 'datagrid');
	$id = isset($_GET["id"]) ? trim($_GET["id"]) : (isset($_POST["id"]) ? trim($_POST["id"]) : 0);
	$value = isset($_POST["value"]) ? trim($_POST["value"]) : isset($_GET["value"]) ? trim($_GET["value"]) : '';
	$page = isset($_POST['iDisplayStart']) ? intval($_POST['iDisplayStart']) : 0;
	$rows = isset($_POST['iDisplayLength']) ? intval($_POST['iDisplayLength']) : 25;
	$sort = isset($_POST["iSortTitle_0"]) ? $_POST["iSortTitle_0"] : NULL;
	$order = isset($_POST["sSortDir_0"]) ? $_POST["sSortDir_0"] : NULL;
	$chr = isset($_POST["sSearch"]) ? $_POST["sSearch"] : NULL;
	$sEcho = isset($_POST['sEcho']) ? $_POST['sEcho'] : 1;
	$user_type = ((isset($_POST['user_type']) && $_POST['user_type']>0) ? $_POST['user_type'] : 0);
	$status_filter = isset($_POST['status_filter']) ? $_POST['status_filter'] :'';

	extract($_GET);
	$searchArray = array("page"=>$page, "rows"=>$rows, "sort"=>$sort, "order"=>$order, "offset"=>$page, "chr"=>$chr, 'sEcho' =>$sEcho,'user_type'=>$user_type,'status_filter'=>$status_filter);

	$mainObject = new Redeem($module, $id, NULL, $searchArray, $action);

	if($action == "updateStatus") {
		$db->update($table,array('isactive'=>($value=='y'?'y':'n')),array("id"=>$id));
		echo json_encode(array('type'=>'success','Record '.($value == 'y' ? 'activated ' : 'deactivated ').'successfully'));
		exit;
	}else if($action == "delete") {
		$db->delete($table, array('id'=>$id));
	} else if($action=="ApproveRedeem" && !empty($id)) {
		$a = $mainObject->getPayPalRedeemLink($id);
		echo json_encode($a);
		exit;
	}else if($action=="ApproveRedeemSafari" && !empty($id)) {		

		$redeem_details = $db->pdoQuery('
			SELECT r.*, u.paypalEmail, u.walletamount
			FROM tbl_redeem_requests AS r INNER JOIN tbl_users AS u ON(u.userId=r.userId)
			WHERE r.id=? AND r.paymentStatus=?', array($id, 'pending'))->result();

		$data = array('code' => '100', 'msg' => 'User has not entred safaricom ContactNo');

		extract($redeem_details);

		if ($walletamount >= $amount) {
			if (!empty($safaricomContactNo) && !empty($amount)) {

				$calculatedAdminCommission = $amount * REDEEM_COMMISSION / 100;
				$calculatedAmountToPay = $amount - $calculatedAdminCommission;


				$mpesa= new \Safaricom\Mpesa\Mpesa();

				$initiatorName = SAFARI_INITIATORNAME;
				$securityCredential = SAFARI_CONSUMER_SECRET; 
				$commandID = SAFARI_COMMANDID;
				$partyA = SAFARI_PARTYA;
				$remarks = SAFARI_REMARKS;
				$occasion = SAFARI_REMARKS;
				$amount = round(exchange($calculatedAmountToPay,'USD','KES'));
				$partyB = $safaricomContactCode.$safaricomContactNo;
				$queueTimeOutURL = ADMIN_URL."modules-nct/redeem_request-nct/result.php";//ADMIN_URL.'modules-nct/redeem_request-nct/result.php';
				$resultURL = ADMIN_URL."modules-nct/redeem_request-nct/result.php";//ADMIN_URL.'modules-nct/redeem_request-nct/result.php';

				$b2cTransaction=$mpesa->b2c($initiatorName, $securityCredential, $commandID, $amount, $partyA, $partyB, $remarks, $queueTimeOutURL, $resultURL, $occasion);

				$b2cTransaction = json_decode($b2cTransaction);
				
				if(isset($b2cTransaction->ResponseCode) && $b2cTransaction->ResponseCode == '0'){
					$db->update('tbl_redeem_requests',
						array(
							'paymentStatus'=>'initiated',
							'conversation_id'=>$b2cTransaction->ConversationID
						),array('id'=>$id));

					// //make entry of pending transaction
					$payment_history_id = $db->insert('tbl_payment_history', 
						array(
							'userId' => $userId, 
							'paymentType' => 'redeem', 
							'membershipId' => $id, 
							'createdDate' => date('Y-m-d H:i:s'), 
							'adminCommission' => (string) $calculatedAdminCommission,
							'paymentStatus' => 'initiated',
							'conversation_id'=> $b2cTransaction->ConversationID,
							'gateway' => 'sa'
						))->lastInsertId();

					$msgType = $_SESSION["toastr_message"] = disMessage(array(
						'type' => 'suc',
						'var' => $b2cTransaction->ResponseDescription
					));					
					$data['code'] = '200';
					$data['type'] = 'success';
					$data['url'] = ADMIN_URL.'modules-nct/redeem_request-nct/';
					$data['msg'] = $b2cTransaction->ResponseDescription;
				}
				else{	
					$data['code'] = '100';		
					$data['url'] = ADMIN_URL.'modules-nct/redeem_request-nct/';
					$data['msg'] = $b2cTransaction->errorMessage;	
				}
			}
		} else {
			$data['msg'] = "Currently user doesn't have sufficient balance to redeem";
		}
		echo json_encode($data);
		exit;
	}
	extract($mainObject->data);
	echo ($content);
	exit;