<?php
	header("Cache-Control: no cache");
	session_cache_limiter("private_no_expire"); 
	error_reporting(1);
	extract($_POST);	
	
	require_once ("../../../../includes-nct/config-nct.php");	
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		  <title>Safaricom Security Credentials</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h2>Safaricom Security Credentials</h2>
			<form class="form-horizontal" method="post" name="b2cform" autocomplete="off">
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="initiator security password">Initiator security password:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($ISP)?$ISP:'' ?>" required id="ISP" placeholder="Enter initiator security password" name="ISP">
			      	</div>
			    </div>			    
			    <div class="form-group">        
			      	<div class="col-sm-offset-2 col-sm-10">
			        	<button type="submit" value="submitSecurityCredentials" name="submit" class="btn btn-default">Submit</button>
			      	</div>
			    </div>
			</form>

		</div>
	</body>
</html>
<?php		
	


	if(isset($submit) && $submit == 'submitSecurityCredentials'){

		function encryptCredentials($source){
        
	        $fp = fopen('cert.cer',"r");

	        $cert_data = fread($fp,8192);

	        fclose($fp);

	        $cert = openssl_x509_read($cert_data);

	        $pub_key = openssl_get_publickey($cert);

	        openssl_public_encrypt($source,$crypt_text, $pub_key,OPENSSL_PKCS1_PADDING );

	        return base64_encode($crypt_text);
	    }

		//$publicKey = ADMIN_URL.'modules-nct/redeem_request-nct/demo/cert.cer';
		//$publicKey = '';
		//$plaintext = "Safaricom132!";
		//$plaintext = $ISP;

		//openssl_public_encrypt($plaintext, $encrypted, $publicKey, OPENSSL_PKCS1_PADDING);
		//$response = base64_encode($encrypted);		

	    $response = encryptCredentials($ISP);

		echo "<div class='container'><h2>Safaricom Security Credentials Response</h2><textarea class='form-control' rows='5'>".$response."</textarea></div>";
		
	}
?>