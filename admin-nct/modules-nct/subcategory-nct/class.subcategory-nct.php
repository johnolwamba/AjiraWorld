<?php
class subCategory extends Home {
	
	public $constantValue;
	public $constantName;
	public $data = array();
	
	public function __construct($module,$id=0, $searchArray=array(), $type='') {		
		global $db, $fields, $sessCataId;
        $this->db = $db;
        $this->data['id'] = $this->id = $id;
        $this->fields = $fields;
        $this->module = $module;
        $this->table = 'tbl_categories';

        $this->type = ($this->id > 0 ? 'edit' : 'add');
        $this->searchArray = $searchArray;

		parent::__construct();				
		if($this->id>0){
			$qrySel = $this->db->select($this->table, array("id","cateName","description","isActive"),array("id"=>$id))->result();

			$fetchRes = $qrySel;
			$this->data['id'] = $this->id =$fetchRes['id'];
			$this->data['cateName'] = $this->cateName =$fetchRes['cateName'];
			$this->data['description'] = $this->description =$fetchRes['description'];
			$this->data['isActive'] = $this->isActive =$fetchRes['isActive'];
		}
		else{
			$this->data["id"] = $this->id = '';
			$this->data["cateName"] = $this->cateName = '';
			$this->data["description"] = $this->description = '';
			$this->data["isActive"] = $this->isActive = '';
		}
		
		switch($type){
			case 'add' : {
				$this->data['content'] =  (in_array('add',$this->Permission))?$this->getForm():'';
				break;
			}
			case 'edit' : {
				$this->data['content'] =  (in_array('edit',$this->Permission))?$this->getForm():'';
				break;
			}
			case 'view' : {
				$this->data['content'] =  '';
				break;
			}
			case 'delete' : {
				$this->data['content'] = (in_array('delete',$this->Permission))?json_encode($this->dataGrid()):'';
				break;
			}
			case 'datagrid' :  {
				$this->data['content'] = (in_array('module',$this->Permission))?json_encode($this->dataGrid()):'';
			}
		}

	}
	
	public function getForm() {
		$content='';
		$category_opt= new MainTemplater(DIR_ADMIN_TMPL.$this->module."/option-nct.tpl.php");
		$category_opt = $category_opt->parse();
		$fields = array('%VALUE%','%SELECTED%','%LABEL%');
		$selCats = $this->db->select('tbl_categories',array('id','cateName'),array('isActive'=>'y','parentId'=>0))->results();
		$category_name_field = NULL;
		foreach ($selCats as $category) {
			$selected = ($this->id == $category['id'] ? "selected='selected'" : '');
			$fields_replace = array($category['id'],$selected,$category['cateName']);
			$category_name_field .= str_replace($fields,$fields_replace,$category_opt);
		}			

		$main_content = new MainTemplater(DIR_ADMIN_TMPL.$this->module."/form-nct.tpl.php");
		$main_content = $main_content->parse();
		$status_a=($this->isActive == 'y' ? 'checked':'');
		$status_d=($this->isActive != 'y' ? 'checked':'');
        //dump_exit($this->cateName);    
		$fields = array("%CATEGORY_NAME_FIELD%","%SUBCATEGORY_NAME%","%DESCRIPTION%","%STATUS_A%","%STATUS_D%","%TYPE%","%ID%");	
		$fields_replace = array($category_name_field,$this->cateName,$this->description,$status_a,$status_d,$this->type,$this->id);	
		$content=str_replace($fields,$fields_replace,$main_content);
		return sanitize_output($content);
	}
	
	public function dataGrid() {
		$content = $operation = $whereCond = $whereCond1= $totalRow = NULL;
		$result = $tmp_rows = $row_data = array();
		extract($this->searchArray);
		$chr = isset($chr)?str_replace(array('_', '%'), array('\_', '\%'),$chr ) : '';
		$whrCond = '';
		/*$whrArr = array();*/
		if(isset($chr) && $chr != '') {
			$whrCond =" AND sub.cateName LIKE '%".$chr."%' OR sub.description LIKE '%".$chr."%'";
			
		}		
		if(isset($sort))
			$sorting = $sort.' '. $order;
		else
			$sorting = 'sub.cateName DESC';	
		

		$qry = 'SELECT sub.id, sub.cateName AS subcateName, sub.description, sub.isActive, sub.id, cat.cateName FROM tbl_categories AS sub INNER JOIN tbl_categories AS cat ON (sub.parentId = cat.id) WHERE 1'.$whrCond. ' ORDER BY '.$sorting.' limit ' . $offset . ' ,' . $rows . ' ';
		$results = $this->db->pdoQuery($qry)->results();
		
		$totalRow = $this->db->pdoQuery('SELECT sub.id FROM tbl_categories AS sub INNER JOIN tbl_categories AS cat ON (sub.parentId = cat.id) WHERE 1'.$whrCond)->affectedRows(); 
		foreach($results as $fetchRes) {
			$status = ($fetchRes['isActive']=="y") ? "checked" : "";
			$id = $fetchRes['id'];			
			$switch  =(in_array('status',$this->Permission))?$this->toggel_switch(array("action"=>"ajax.".$this->module.".php?id=".$id."","check"=>$status)):'';			
			$operation =(in_array('edit',$this->Permission))?'&nbsp;&nbsp;'.$this->operation(array("href"=>SITE_ADM_MOD.$this->module."/ajax.".$this->module.".php?action=edit&id=".$id,"class"=>"btn default btn-xs black btnEdit","value"=>'<i class="fa fa-edit"></i>&nbsp;Edit')):'';			
			$operation .=(in_array('delete',$this->Permission))?'&nbsp;&nbsp;'.$this->operation(array("href"=>"ajax.".$this->module.".php?action=delete&id=".$id."","class"=>"btn default btn-xs red btn-delete","value"=>'<i class="fa fa-trash-o"></i>&nbsp;Delete')):'';
			$final_array = array($id,stripslashes($fetchRes["subcateName"]),stripslashes($fetchRes["cateName"]),stripslashes($fetchRes['description']));
			if(in_array('status',$this->Permission)){
				$final_array =  array_merge($final_array, array($switch));
			}	
			if(in_array('edit',$this->Permission) || in_array('delete',$this->Permission) || in_array('view',$this->Permission) ){ 		
				$final_array =  array_merge($final_array, array($operation));
			}
				
			$row_data[] = $final_array;			
		}
		$result["sEcho"]=$sEcho;
		$result["iTotalRecords"] = (int)$totalRow;
		$result["iTotalDisplayRecords"] = (int)$totalRow;
		$result["aaData"] = $row_data;
		return $result;	
	
	}
	public function toggel_switch($text){
		$text['action'] = isset($text['action']) ? $text['action'] : 'Enter Action Here: ';
		$text['check'] = isset($text['check']) ? $text['check'] : '';
		$text['name'] = isset($text['name']) ? $text['name'] : '';
        $text['class'] = isset($text['class']) ? ''.trim($text['class']) : '';
        $text['extraAtt'] = isset($text['extraAtt']) ? $text['extraAtt'] : '';
	
		$main_content = new MainTemplater(DIR_ADMIN_TMPL.$this->module.'/switch-nct.tpl.php');
		$main_content=$main_content->parse();
		$fields = array("%NAME%","%CLASS%","%ACTION%","%EXTRA%","%CHECK%");
		$fields_replace = array($text['name'],$text['class'],$text['action'],$text['extraAtt'],$text['check']);
		return str_replace($fields,$fields_replace,$main_content);	
	}
	public function operation($text){
		
		$text['href'] = isset($text['href']) ? $text['href'] : 'Enter Link Here: ';
		$text['value'] = isset($text['value']) ? $text['value'] : '';
		$text['name'] = isset($text['name']) ? $text['name'] : '';
        $text['class'] = isset($text['class']) ? ''.trim($text['class']) : '';
        $text['extraAtt'] = isset($text['extraAtt']) ? $text['extraAtt'] : '';
		$main_content = new MainTemplater(DIR_ADMIN_TMPL.$this->module.'/operation-nct.tpl.php');
		$main_content=$main_content->parse();
		$fields = array("%HREF%","%CLASS%","%VALUE%","%EXTRA%");
		$fields_replace = array($text['href'],$text['class'],$text['value'],$text['extraAtt']);
		return str_replace($fields,$fields_replace,$main_content);
	}
	public function getPageContent(){
		$final_result = NULL;
		$main_content = new MainTemplater(DIR_ADMIN_TMPL.$this->module."/".$this->module.".tpl.php");
		$main_content->breadcrumb = $this->getBreadcrumb();
		$main_content->getForm = $this->getForm();
		$final_result = $main_content->parse();
		return $final_result;
	}
}