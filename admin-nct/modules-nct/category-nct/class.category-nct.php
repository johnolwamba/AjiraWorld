<?php
class Category extends Home {
	
	public $constantValue;
	public $constantName;
	public $data = array();
	
	public function __construct($module,$id=0, $searchArray=array(), $type='') {		
		global $db, $fields, $sessCataId;
        $this->db = $db;
        $this->data['id'] = $this->id = $id;
        $this->fields = $fields;
        $this->module = $module;
        $this->table = 'tbl_categories';

        $this->type = ($this->id > 0 ? 'edit' : 'add');
        $this->searchArray = $searchArray;
		parent::__construct();				
		if($this->id>0){
			$qrySel = $this->db->select($this->table, array("id","cateName","description","createdDate","isActive"),array("id"=>$id))->result();
			$fetchRes = $qrySel;
			$this->data['id'] = $this->id = $fetchRes['id'];
			$this->data["description"] = $this->description = $fetchRes["description"];
			$this->data['cateName'] = $this->cateName =  $fetchRes['cateName'];
			$this->data['isActive'] = $this->isActive = $fetchRes['isActive'];
		}
		else{
			$this->data['id'] = $this->id = '';
			$this->data["description"] = $this->description = '';
			$this->data['cateName'] = $this->cateName = '';
			$this->data['isActive'] = $this->isActive = '';
		}
		
		switch($type){
			case 'add' : {
				$this->data['content'] =  (in_array('add',$this->Permission))?$this->getForm():'';
				break;
			}
			case 'import_excel' : {
				$this->data['content'] =  (in_array('import',$this->Permission))?$this->getImportCsvForm():'';
				break;
			}
			case 'edit' : {
				$this->data['content'] =  (in_array('edit',$this->Permission))?$this->getForm():'';
				break;
			}
			case 'view' : {
				$this->data['content'] =  '';
				break;
			}
			case 'delete' : {
				$this->data['content'] = (in_array('delete',$this->Permission))?json_encode($this->dataGrid()):'';
				break;
			}
			case 'datagrid' :  {
				$this->data['content'] = (in_array('module',$this->Permission))?json_encode($this->dataGrid()):'';
			}
		}

	}
	public function viewForm() {
        $content = $this->fields->displayBox(array("label" => "Category Name&nbsp;:", "value" => $this->subject)) .
                $content = $this->fields->displayBox(array("label" => "Templates&nbsp;:", "value" => $this->templates));
        return $content;
    }
	public function getForm() {
		$content='';
			$main_content = new MainTemplater(DIR_ADMIN_TMPL.$this->module."/form-nct.tpl.php");
			$main_content = $main_content->parse();
            $status_a=($this->isActive == 'y' ? 'checked':'');
			$status_d=($this->isActive != 'y' ? 'checked':'');
	
			$fields = array("%CATEGORY_NAME%","%DESCRIPTION%","%STATUS_A%","%STATUS_D%","%TYPE%","%ID%");
	
			$fields_replace = array($this->cateName,$this->description,$status_a,$status_d,$this->type,$this->id);
	
			$content=str_replace($fields,$fields_replace,$main_content);
		return sanitize_output($content);
	}
	
	public function dataGrid() {
		$content = $operation = $whereCond = $whereCond1= $totalRow = NULL;
		$result = $tmp_rows = $row_data = array();
		extract($this->searchArray);
		$langId = isset($langId)?$langId:1;
		$chr = isset($chr)?str_replace(array('_', '%',"'"), array('\_', '\%',"\'"),$chr ) : '';
		$whrCond = '';
		/*$whrArr = array();*/
		if(isset($chr) && $chr != '') {
			$whrCond =" AND cateName LIKE '%".$chr."%' OR  description LIKE '%".$chr."%'";
			
		}		
		if(isset($sort))
			$sorting = $sort.' '. $order;
		else
			$sorting = 'id DESC';			



		$qry = 'SELECT id,cateName,description, isActive FROM tbl_categories WHERE 1 AND parentId = 0 '.$whrCond.' ORDER BY '.$sorting;

		/////////////////
		$totalRows = $this->db->pdoQuery($qry)->results();
		$totalRow = count($totalRows);
		
        $query_with_limit = $qry . " LIMIT " . $offset . " ," . $rows . " ";
        $results = $this->db->pdoQuery($query_with_limit)->results();

        ///////////


		$results = $this->db->pdoQuery($qry)->results();
		
		
		foreach($results as $fetchRes) {
			$status = ($fetchRes['isActive']=="y") ? "checked" : "";
			$id = $fetchRes['id'];
						
			$switch  =(in_array('status',$this->Permission))?$this->toggel_switch(array("action"=>"ajax.".$this->module.".php?id=".$id."","check"=>$status)):'';			
			
			$operation =(in_array('edit',$this->Permission))?'&nbsp;&nbsp;'.$this->operation(array("href"=>SITE_ADM_MOD.$this->module."/ajax.".$this->module.".php?action=edit&id=".$id,"class"=>"btn default btn-xs black btnEdit","value"=>'<i class="fa fa-edit"></i>&nbsp;Edit')):'';
			
			$operation .=(in_array('delete',$this->Permission))?'&nbsp;&nbsp;'.$this->operation(array("href"=>"ajax.".$this->module.".php?action=delete&id=".$fetchRes['id']."","class"=>"btn default btn-xs red btn-delete","value"=>'<i class="fa fa-trash-o"></i>&nbsp;Delete')):'';
			$final_array = array($id,stripslashes($fetchRes["cateName"]),stripslashes($fetchRes['description']));
			if(in_array('status',$this->Permission)){
				$final_array =  array_merge($final_array, array($switch));
			}	
			if(in_array('edit',$this->Permission) || in_array('delete',$this->Permission) || in_array('view',$this->Permission) ){ 		
				$final_array =  array_merge($final_array, array($operation));
			}
				
			$row_data[] = $final_array;			
		}
		$result["sEcho"]=$sEcho;
		$result["iTotalRecords"] = (int)$totalRow;
		$result["iTotalDisplayRecords"] = (int)$totalRow;
		$result["aaData"] = $row_data;
		return $result;	
	
	}
	
	public function toggel_switch($text){
		$text['action'] = isset($text['action']) ? $text['action'] : 'Enter Action Here: ';
		$text['check'] = isset($text['check']) ? $text['check'] : '';
		$text['name'] = isset($text['name']) ? $text['name'] : '';
        $text['class'] = isset($text['class']) ? ''.trim($text['class']) : '';
        $text['extraAtt'] = isset($text['extraAtt']) ? $text['extraAtt'] : '';
	
		$main_content = new MainTemplater(DIR_ADMIN_TMPL.$this->module.'/switch-nct.tpl.php');
		$main_content=$main_content->parse();
		$fields = array("%NAME%","%CLASS%","%ACTION%","%EXTRA%","%CHECK%");
		$fields_replace = array($text['name'],$text['class'],$text['action'],$text['extraAtt'],$text['check']);
		return str_replace($fields,$fields_replace,$main_content);	
	}
	public function operation($text){
		
		$text['href'] = isset($text['href']) ? $text['href'] : 'Enter Link Here: ';
		$text['value'] = isset($text['value']) ? $text['value'] : '';
		$text['name'] = isset($text['name']) ? $text['name'] : '';
        $text['class'] = isset($text['class']) ? ''.trim($text['class']) : '';
        $text['extraAtt'] = isset($text['extraAtt']) ? $text['extraAtt'] : '';
		$main_content = new MainTemplater(DIR_ADMIN_TMPL.$this->module.'/operation-nct.tpl.php');
		$main_content=$main_content->parse();
		$fields = array("%HREF%","%CLASS%","%VALUE%","%EXTRA%");
		$fields_replace = array($text['href'],$text['class'],$text['value'],$text['extraAtt']);
		return str_replace($fields,$fields_replace,$main_content);
	}
	public function getPageContent(){
		$final_result = NULL;
		$main_content = new MainTemplater(DIR_ADMIN_TMPL.$this->module."/".$this->module.".tpl.php");
		$main_content->breadcrumb = $this->getBreadcrumb();
		$main_content->getForm = $this->getForm();
		
		$final_result = $main_content->parse();
		return $final_result;
	}
}