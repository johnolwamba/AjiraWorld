<?php
	error_reporting(E_ALL);			
	header("Cache-Control: no cache");
	session_cache_limiter("private_no_expire"); 
	error_reporting(0);
	extract($_POST);	
	
	define('SAFARI_CONSUMER_KEY', 'mcecSE5TT3H5B8A9P3osAQlUTsKTL03N');
    define('SAFARI_CONSUMER_SECRET', 'lWFwlmeUdrOQ2G5u');
	
	$_SESSION['SAFARI_CONSUMER_KEY'] = SAFARI_CONSUMER_KEY;
	$_SESSION['SAFARI_CONSUMER_SECRET'] = SAFARI_CONSUMER_SECRET;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		  <title>Safaricom Authentication</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h2>Safaricom Authentication</h2>
			<form class="form-horizontal" method="post" name="b2cform" autocomplete="off">
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="ConsumerKey">Consumer Key:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= $_SESSION['SAFARI_CONSUMER_KEY'] ?>" required id="ConsumerKey" placeholder="Enter ConsumerKey" name="ConsumerKey">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="ConsumerSecret">Consumer Secret:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= $_SESSION['SAFARI_CONSUMER_SECRET'] ?>" required id="ConsumerSecret" placeholder="Enter ConsumerSecret" name="ConsumerSecret">
			      	</div>
			    </div>
			    <div class="form-group">        
			      	<div class="col-sm-offset-2 col-sm-10">
			        	<button type="submit" value="submitAuthentication" name="submit" class="btn btn-default">Submit</button>
			      	</div>
			    </div>
			</form>

		</div>
	</body>
</html>
<?php		
	if(isset($submit) && $submit == 'submitAuthentication'){

		$url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		$credentials = base64_encode("{$ConsumerKey}:{$ConsumerSecret}");
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		$curl_response = curl_exec($curl);
                var_dump($curl_response);
		echo "<div class='container'><h2>Safaricom Authentication Response</h2><pre>".$curl_response."</pre></div>";
		
	}
?>
