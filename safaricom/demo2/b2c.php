<?php
	header("Cache-Control: no cache");
	session_cache_limiter("private_no_expire"); 
	error_reporting(E_ALL);

	extract($_POST);	
	
	//require_once ("../../../../includes-nct/config-nct.php");
	define('SAFARI_CONSUMER_KEY', 'mcecSE5TT3H5B8A9P3osAQlUTsKTL03N');
    define('SAFARI_CONSUMER_SECRET', 'lWFwlmeUdrOQ2G5u');
	$_SESSION['SAFARI_CONSUMER_KEY'] = SAFARI_CONSUMER_KEY;
	$_SESSION['SAFARI_CONSUMER_SECRET'] = SAFARI_CONSUMER_SECRET;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		  <title>Safaricom B2C</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h2>Safaricom B2C</h2>
			<form class="form-horizontal" method="post" name="b2cform" autocomplete="off">
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="accessToken">Access Token:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($accessToken)?$accessToken:'' ?>" required id="accessToken" placeholder="Enter accessToken" name="accessToken">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="InitiatorName">InitiatorName:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($InitiatorName)?$InitiatorName:'' ?>" required id="InitiatorName" placeholder="Enter InitiatorName" name="InitiatorName">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="SecurityCredential">SecurityCredential:</label>
			      	<div class="col-sm-10">
			      		<textarea class="form-control" required id="SecurityCredential" placeholder="Enter SecurityCredential" rows="5" name="SecurityCredential"><?= isset($SecurityCredential)?$SecurityCredential:'' ?></textarea>			        	
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="CommandID">CommandID:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($CommandID)?$CommandID:'' ?>" required id="CommandID" placeholder="Enter CommandID" name="CommandID">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="Amount">Amount:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($Amount)?$Amount:'' ?>" required id="Amount" placeholder="Enter Amount" name="Amount">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="PartyA">PartyA:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($PartyA)?$PartyA:'' ?>" required id="PartyA" placeholder="Enter PartyA" name="PartyA">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="PartyB">PartyB:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($PartyB)?$PartyB:'' ?>" required id="PartyB" placeholder="Enter PartyB" name="PartyB">
			      	</div>
			    </div>
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="Remarks">Remarks:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($Remarks)?$Remarks:'' ?>" required id="Remarks" placeholder="Enter Remarks" name="Remarks">
			      	</div>
			    </div>			    
			    <div class="form-group">
			      	<label class="control-label col-sm-2" for="Occasion">Occasion:</label>
			      	<div class="col-sm-10">
			        	<input type="text" class="form-control" value="<?= isset($Occasion)?$Occasion:'' ?>" required id="Occasion" placeholder="Enter Occasion" name="Occasion">
			      	</div>
			    </div>
			    <div class="form-group">        
			      	<div class="col-sm-offset-2 col-sm-10">
			        	<button type="submit" value="submitB2C" name="submit" class="btn btn-default">Submit</button>
			      	</div>
			    </div>
			</form>
			<div class="form-group">
		      	<label class="control-label" for="QueueTimeOutURL">QueueTimeOutURL :</label> https://dev.ajira.world/safaricom/demo2/result.php


		    </div>

		    <div class="form-group">
		      	<label class="control-label" for="ResultURL">ResultURL :</label> https://dev.ajira.world/safaricom/demo2/result.php
		    </div>

		    <div class="form-group">
		      	<label class="control-label" for="ResultURL">ResultURL Response :</label> https://dev.ajira.world/safaricom/demo2/notify.txt
		    </div>
		</div>
	</body>
</html>
<?php		
	if(isset($submit) && $submit == 'submitB2C'){
print_r($_REQUEST);
		$url = 'https://sandbox.safaricom.co.ke/mpesa/b2c/v1/paymentrequest';

	       
                $curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$accessToken.'')); //setting custom header
                
		$curl_post_data = array(
		  //Fill in the request parameters with valid values
		  'InitiatorName' => $InitiatorName,
		  'SecurityCredential' => $SecurityCredential,
		  'CommandID' => $CommandID,
		  'Amount' => $Amount,
		  'PartyA' => $PartyA,
		  'PartyB' => $PartyB,
		  'Remarks' => $Remarks,
		  'QueueTimeOutURL' => 'https://dev.ajira.world/safaricom/demo2/result.php',
		  'ResultURL' => 'https://dev.ajira.world/safaricom/demo2/result.php',
		  'Occasion' => $Occasion
		);
               
		$data_string = json_encode($curl_post_data);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

		$curl_response = curl_exec($curl);
                
		echo "<div class='container'><h2>Safaricom B2C Response</h2><pre>".$curl_response."</pre></div>";
		
	}
?>
