<?php
require 'includes-nct/vendor/autoload.php';

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

$httpClient = new GuzzleAdapter(new Client());
$sparky = new SparkPost($httpClient, ['key'=>'8154e5e085f6cb66616e2b5e69021fd05f105766']);

$promise = $sparky->transmissions->post([
    'content' => [
        'from' => [
            'name' => 'Ajira World',
            'email' => 'ananda@ajira.world',
        ],
        'subject' => 'First Mailing From PHP',
        'html' => '<html><body><h1>Congratulations, {{name}}!</h1><p>You just sent your very first mailing!</p></body></html>',
        'text' => 'Congratulations, {{name}}!! You just sent your very first mailing!',
    ],
    'substitution_data' => ['name' => 'Johnstone'],
    'recipients' => [
        [
            'address' => [
                'name' => 'Johnstone Ananda',
                'email' => 'johnolwamba@gmail.com',
            ],
        ],
    ],
    'cc' => [
        [
            'address' => [
                'name' => 'Hitesh',
                'email' => 'hitesh.parekh@ncrypted.com',
            ],
        ],
    ],
    'bcc' => [
        [
            'address' => [
                'name' => 'Johnstone Ananda',
                'email' => '=johnstone.ananda@strathmore.edu',
            ],
        ],
    ],
]);

try {
    $response = $promise->wait();
    echo $response->getStatusCode()."\n";
    print_r($response->getBody())."\n";
} catch (\Exception $e) {
    echo $e->getCode()."\n";
    echo $e->getMessage()."\n";
}
//thats all u need...you put that in a function and pass parameters replace it with the sendEmailAddress function that you are using
?>
